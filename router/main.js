const patterns = require('./patterns')

const routes = [
  {
    pattern: '/',
    name: 'home',
    page: 'index',
  },
  {
    pattern: '/mycourse',
    name: 'my-course',
  },
  {
    pattern: '/course',
    name: 'course',
  },
  {
    pattern: '/payment',
    name: 'payment',
  },
  {
    pattern: '/thankyou',
    name: 'thank-you',
  },
  {
    pattern: '/reset-password',
    name: 'reset-password',
  },
  {
    pattern: '/bank-transfer',
    name: 'bank-transfer',
  },
  {
    pattern: '/profile',
    name: 'profile',
  },
  {
    pattern: '/about',
    name: 'static-about',
  },
  {
    pattern: '/privacy',
    name: 'term',
  },
  {
    pattern: '/support',
    name: 'support',
  },
  {
    pattern: `/:slug(${patterns.slug})`,
    name: 'institute',
  },
]

module.exports = routes
