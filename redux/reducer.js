import { combineReducers } from 'redux'
import account from '@reducers/account'
import register from '@reducers/register'
import modal from '@reducers/modal'
import isLoading from '@reducers/isLoading'
import courses from '@reducers/courses'
import booking from '@reducers/booking'
import isAlert from '@reducers/isAlert'
import myCourse from '@reducers/myCourse'
import order from '@reducers/order'

const rootReducer = combineReducers({
  account,
  register,
  modal,
  isLoading,
  courses,
  booking,
  isAlert,
  myCourse,
  order,
})
export default rootReducer
