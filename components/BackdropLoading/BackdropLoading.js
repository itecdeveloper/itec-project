import React from 'react'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'

import BackdropLoadingStyles from './BackdropLoadingStyles'

function BackdropLoading({ open }) {
  const classes = BackdropLoadingStyles()
  return (
    <Backdrop className={classes.backdrop} open={open}>
      <CircularProgress color="inherit" />
    </Backdrop>
  )
}

export default BackdropLoading
