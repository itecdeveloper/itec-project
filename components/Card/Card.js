import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import CardStyles from './CardStyled'
import IconButton from '@material-ui/core/IconButton'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline'

import Link from '@components/Link'

import { getStatic } from '@lib/static'

import classNames from 'classnames'

function CardComponent({
  title,
  detail,
  image,
  page,
  bgColor,
  slugId,
  className,
  contentClassName,
}) {
  const classes = CardStyles({ bgColor })
  const cardClasses = classNames({
    [classes.root]: true,
    [className]: className,
  })
  const cardContentClasses = classNames({
    [classes.cardContent]: true,
    [contentClassName]: contentClassName,
  })

  return (
    <Card className={cardClasses}>
      <CardMedia className={classes.media} image={image} title="Paella dish" />
      <Link route={page} params={{ id: slugId }} passHref>
        <CardActionArea>
          <CardContent classes={{ root: cardContentClasses }}>
            <div className={classes.typographyRoot}>
              <Typography variant="h5" classes={{ h5: classes.typographyH5 }}>
                {title}
              </Typography>
              <IconButton aria-label="delete" className={classes.iconButton}>
                <PlayCircleOutlineIcon fontSize="inherit" />
              </IconButton>
            </div>
            <div className={classes.bodyRoot}>
              <Typography
                variant="body1"
                component="p"
                classes={{ body1: classes.typographyBody1 }}
              >
                {detail}
              </Typography>
            </div>
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  )
}

CardComponent.defaultProps = {
  title: 'lorem ipsum',
  detail:
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
  image: getStatic('static/images/image_card.png'),
  page: 'home',
  id: 1,
}

export default CardComponent
