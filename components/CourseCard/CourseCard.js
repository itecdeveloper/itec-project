import React, { Fragment } from 'react'
import { splitCourseName } from '@util/dataFormate'

import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline'
import Grid from '@material-ui/core/Grid'

import CourseCardStyles from './CourseCardStyles'

import Link from '@components/Link'
import Icon from '@components/Icon'

import { getStatic } from '@lib/static'

import classNames from 'classnames'

function CourseCard({ title, detail, image, page, slugId, levels }) {
  const classes = CourseCardStyles()
  const uniqueLevels = levels
    .filter(((set) => (f) => !set.has(f.LEVEL) && set.add(f.LEVEL))(new Set()))
    .filter((i) => !i.HIDE)

  return (
    <Link route={page} params={{ id: slugId }} passHref>
      <div className={classes.root}>
        <div className={classes.cardLayout}>
          <img className={classes.avatar} src={image} alt="img" />

          <Link route={page} params={{ id: slugId }} passHref>
            <div className={classes.whiteSheet}>
              <div className={classes.typographyRoot}>
                <Typography variant="h5" classes={{ h5: classes.typographyH5 }}>
                  {splitCourseName(title)[0]}
                </Typography>
                {/* <IconButton aria-label="delete" className={classes.iconButton}>
                <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>arrowForwardCircle</Icon>
              </IconButton> */}
              </div>
              {splitCourseName(title)[1] && (
                <div className={classes.typographyRoot}>
                  <Typography variant="h6" classes={{ h6: classes.typographyH6 }}>
                    {splitCourseName(title)[1]}
                  </Typography>
                </div>
              )}

              {/* <div className={classes.bodyRoot}>
              <Typography
                variant="body1"
                component="p"
                classes={{ body1: classes.typographyBody1 }}
              >
                {detail}
              </Typography>
            </div> */}
            </div>
          </Link>

          <Link route={page} params={{ id: slugId }} passHref>
            <div className={classNames(classes.getCourseLayout, 'get-course-layout')}>
              {uniqueLevels.length > 0 && (
                <Grid container>
                  {uniqueLevels.map((level, index) => (
                    <Fragment key={index}>
                      <Grid item xs={3}>
                        <div className={classes.getCourseText}>Level</div>
                      </Grid>
                      <Grid item xs={9}>
                        <div className={classes.getCourseText}>{level.LEVEL}</div>
                      </Grid>
                    </Fragment>
                  ))}
                </Grid>
              )}
            </div>
          </Link>
        </div>
      </div>
    </Link>
  )
}

CourseCard.defaultProps = {
  title: 'lorem ipsum',
  detail:
    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
  image: getStatic('static/images/image_card.png'),
  page: 'home',
  id: 1,
}

export default CourseCard
