import { makeStyles } from '@material-ui/core/styles'

const CourseCardStyles = makeStyles((theme) => ({
  root: {
    height: '313px',
    width: '360px',
    maxHeight: '313px',
    minHeight: '313px',
    borderRadius: '5px',
    backgroundColor: '#ffffff',
    boxShadow: '0 0 10px 0 #e1e1e1',
    position: 'relative',
    cursor: 'pointer',
    margin: 'auto',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
    // marginRight: '50px',
    // overflow: ${props => !props.recommendedEvents && 'hidden'};
    // '&:hover': {
    //   '& > div': {
    //     '& $whiteSheet': {
    //       top: '0',
    //       height: '90px',
    //       transition: 'all 0.2s linear',
    //       borderTopLeftRadius: '5px',
    //       borderTopRightRadius: '5px',
    //       borderBottomLeftRadius: '0',
    //       borderBottomRightRadius: '0',
    //       backgroundColor: theme.palette.primary.main,
    //       '& $typographyH5, $iconButton': {
    //         color: '#ffffff',
    //       },
    //     },
    //     '& .get-course-layout': {
    //       visibility: 'visible',
    //       position: 'absolute',
    //       bottom: '-111px',
    //       left: 0,
    //       opacity: 1,
    //     },
    //   },
    // },
  },
  cardLayout: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  avatar: {
    // height: '287.06px',
    height: '202.06px',
    width: '360px',
    objectFit: 'cover',
    zIndex: 0,
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  whiteSheet: {
    padding: '16px 16px 16px 24px',
    height: '90px',
    width: '360px',
    backgroundColor: '#ffffff',
    zIndex: 1,
    position: 'absolute',
    // top: '290px',
    top: '202px',
    transition: 'all 0.2s linear',
    borderBottomLeftRadius: '5px',
    borderBottomRightRadius: '5px',
    overflow: 'hidden',
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  getCourseLayout: {
    // display: 'flex',
    padding: '16px 16px 16px 24px',
    opacity: 0,
    position: 'absolute',
    transition: 'all 0.2s linear',
    width: '360px',
    height: '223px',
    borderRadius: '0 0 5px 5px',
    background: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: '-136px',
    left: 0,
    zIndex: 1,
    overflow: 'hidden',
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  getCourseText: {
    color: 'rgba(0, 0, 0, 0.56)',
    fontSize: '14px',
    fontWeight: 600,
    lineHeight: '20px',
    padding: '4px 0',
    // marginRight: '6px',
  },
  typographyRoot: {
    display: 'flex',
    marginBottom: 5,
  },
  typographyH5: {
    color: 'rgba(0, 0, 0, 0.8)',
    lineHeight: '24px',
    flexGrow: 1,
  },
  typographyH6: {
    fontSize: 14,
    color: '#808080',
    lineHeight: '20px',
    flexGrow: 1,
  },
  iconButton: {
    color: theme.palette.primary.main,
    fontSize: '1.4286rem',
    padding: 0,
  },
  bodyRoot: {
    paddingRight: 20,
  },
  typographyBody1: {
    color: 'rgba(0, 0, 0, 0.56)',
    lineHeight: '24px',
  },
}))

export default CourseCardStyles
