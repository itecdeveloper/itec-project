import { makeStyles } from '@material-ui/core/styles'

const SelectStyles = makeStyles((theme) => ({
  root: {},
  menuPaper: {
    marginTop: 8,
    boxShadow: 'inset 0px 0px 4px rgba(0, 0, 0, 0.12)',
    borderRadius: '4px',
    maxHeight: 352,
    overflowY: 'auto',
  },
  menuList: {
    paddingLeft: 8,
    paddingRight: 8,
    // padding: 0,
  },
  menuItem: {
    minHeight: 48,
  },
  icon: {
    color: theme.palette.primary.main,
  },
}))

export default SelectStyles
