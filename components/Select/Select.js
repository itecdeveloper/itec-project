import React, { useState, useEffect } from 'react'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemText from '@material-ui/core/ListItemText'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

import Input from '@components/Input'
import Icon from '@components/Icon'

import SelectStyles from './SelectStyles'

function CustomInput({ ...rest }) {
  return <Input {...rest} outlined />
}

function SelectComponent({ options, label, onChange, value, disabled, ...rest }) {
  const [select, setSelect] = useState(value || '')
  const [open, setOpen] = useState(false)
  const handleChange = (event) => {
    onChange(event.target.value)
  }
  const classes = SelectStyles()

  // useEffect(() => {
  //   if (onChange && typeof onChange === 'function') {
  //     onChange(select)
  //   }
  // }, [select])
  // console.log('SelectComponent', { onChange, value, select })

  return (
    <Select
      value={value}
      onChange={handleChange}
      open={open}
      onOpen={() => {
        setOpen(true)
      }}
      onClose={() => {
        setOpen(false)
      }}
      input={<CustomInput label={label} />}
      renderValue={(selected) => selected}
      MenuProps={{
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'left',
        },
        getContentAnchorEl: null,
        classes: {
          paper: classes.menuPaper,
          list: classes.menuList,
        },
      }}
      disabled={disabled}
      {...rest}
    >
      {options.length > 0 &&
        options.map((option) => (
          <MenuItem className={classes.menuItem} value={option}>
            <ListItemText primary={option} />
            {value === option && <CheckCircleIcon className={classes.icon} />}
          </MenuItem>
        ))}
    </Select>
  )
}

SelectComponent.defaultProps = {
  options: [],
}

export default SelectComponent
