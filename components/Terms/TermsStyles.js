import { makeStyles } from '@material-ui/core/styles'

const Terms = makeStyles((theme) => ({
  root: {
    width: 375,
    fontSize: '14px',
    lineHeight: '160%',
    letterSpacing: '0.02em',
    textTransform: 'capitalize',
    color: 'rgba(0, 0, 0, 0.6)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
}))

export default Terms
