import React, { useState } from 'react'
import DatePicker from 'react-datepicker'

import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

import CalendarStyles from './CalendarStyles'

import moment from 'moment'

function range(from, to) {
  return Array.from({ length: to - from + 1 }, (_v, i) => i + from)
}
const years = range(1975, moment().add(5, 'y').get('year'))
// const years = moment().year()
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

function Calendar({ onChange, value, options, ...rest }) {
  const [startDate, setStartDate] = useState(null)
  // console.log('Calendar', value)
  const handleChange = (date) => {
    onChange(moment(date).format('D MMM YYYY'))
  }
  const renderDayContents = (day, date) => {
    const fomateDate = moment(date).format('D MMM YYYY')
    if (options[fomateDate]) {
      // console.log('renderDayContents', { options: options[fomateDate], date })
      return <span className={classes.renderDayContents}>{day}</span>
    }
    return <span>{day}</span>
  }
  const classes = CalendarStyles()

  return (
    <FormControl className={classes.formControl} fullWidth>
      <div className={classes.root}>
        <DatePicker
          {...rest}
          inline
          selected={value && new Date(moment(value, 'D MMM YYYY'))}
          onChange={handleChange}
          renderDayContents={renderDayContents}
          // calendarContainer={MyContainer}
          renderCustomHeader={({
            date,
            changeYear,
            changeMonth,
            decreaseMonth,
            increaseMonth,
            prevMonthButtonDisabled,
          }) => {
            return (
              <div className={classes.headerRoot}>
                <div>
                  <IconButton className={classes.headerButton} size="small" onClick={decreaseMonth}>
                    {/* <Icons fontSize={12}>back</Icons> */}
                    <ArrowBackIcon />
                  </IconButton>
                </div>
                <div style={{ textAlign: 'center' }} className="react-datepicker__current-month">
                  <select
                    className={classes.customSelect}
                    value={months[moment(date).month()]}
                    onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
                  >
                    {months.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                  <select
                    className={classes.customSelect}
                    value={moment(date).year()}
                    onChange={({ target: { value } }) => changeYear(value)}
                  >
                    {years.map((option) => (
                      <option key={option} value={option}>
                        {option}
                      </option>
                    ))}
                  </select>
                </div>
                <div>
                  <IconButton className={classes.headerButton} size="small" onClick={increaseMonth}>
                    {/* <Icons fontSize={12}>more</Icons> */}
                    <ArrowForwardIcon />
                  </IconButton>
                </div>
              </div>
            )
          }}
        />
      </div>
    </FormControl>
  )
}

export default Calendar
