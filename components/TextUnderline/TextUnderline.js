import React from 'react'
import TextUnderlineStyles from './TextUnderlineStyled'

function TextUnderline({ ...rest }) {
  const classes = TextUnderlineStyles()
  return <p className={classes.root} {...rest} />
}

export default TextUnderline
