import React, { Fragment, useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ListItemText from '@material-ui/core/ListItemText'
import InputAdornment from '@material-ui/core/InputAdornment'
import ErrorIcon from '@material-ui/icons/Error'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import CircularProgress from '@material-ui/core/CircularProgress'

import Input from '@components/Input'
import Icon from '@components/Icon'

import AutocompleteStyles from './AutocompleteStyles'

import classNames from 'classnames'

const patternOption = (label) => {
  return {
    label: label,
    value: label,
  }
}

const funcConvertOptions = (options) => {
  let output = []
  options.map((item) => {
    if (item.label) {
      output = [...output, item]
    } else {
      output = [...output, patternOption(item)]
    }
  })
  return output
}

function CustomInput({ InputProps, inputProps, ...params }) {
  // console.log('CustomInput', { params, InputProps, inputProps })
  return <Input {...params} inputProps={{ ...inputProps, style: { cursor: 'pointer' } }} />
}

function AutocompleteComponent({
  label,
  id,
  name,
  onChange,
  value,
  disabled,
  options,
  loading,
  outlined,
}) {
  const [open, setOpen] = useState(false)
  const classes = AutocompleteStyles()
  const handleChange = (event, newValue) => {
    // console.log('handleChange', newValue)
    onChange(newValue)
  }

  return (
    <Autocomplete
      id={id}
      options={options ? funcConvertOptions(options) : []}
      getOptionLabel={(option) => option && option.label}
      open={open}
      onOpen={() => {
        setOpen(true)
      }}
      onClose={() => {
        setOpen(false)
      }}
      classes={{
        ...classes,
      }}
      onChange={handleChange}
      value={value}
      loading={loading}
      fullWidth
      disabled={disabled}
      renderOption={(option, { selected }) => (
        <Fragment>
          <ListItemText primary={option.label} />
          {selected && <CheckCircleIcon className={classes.icon} />}
        </Fragment>
      )}
      renderInput={(params) => (
        <CustomInput
          inputRef={params.InputProps.ref}
          {...params}
          label={label}
          name={name}
          fullWidth
          outlined={!outlined}
          readOnly
          disabled={disabled}
          endAdornment={
            <div
              className={classNames(
                params.InputProps.endAdornment.props.className,
                classes.endAdornmentIcon,
              )}
            >
              {loading ? <CircularProgress color="inherit" size={20} /> : null}
              {open ? (
                <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>expandMore</Icon>
              ) : (
                <Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>chevronRight</Icon>
              )}
            </div>
          }
        />
      )}
    />
  )
}

export default AutocompleteComponent
