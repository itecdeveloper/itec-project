import React, { useEffect, useState } from 'react'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/modalActions'
import { useRouter } from 'next/router'
import { get } from 'lodash'

import ForgotPassword from '@containers/ForgotPassword'

function getAuthDataFromCallbackURL(query) {
  const urlParams = new URLSearchParams(query)
  const token = urlParams.get('token') || false
  return {
    token,
  }
}

function ResetPassword({ modal, actions }) {
  const router = useRouter()
  const [tokenFromURL, setTokenFromURL] = useState(false)
  const { open } = modal
  const handleClose = () => {
    actions.setOpenModal(!open)
    window.location = '/'
  }

  useEffect(() => {
    const { token: tokenFromURL } = getAuthDataFromCallbackURL(window.location.search)
    if (tokenFromURL) {
      actions.setOpenModal(true)
    }
    setTokenFromURL(tokenFromURL)
    // console.log('tokenFromURL', { tokenFromURL })
  }, [])

  return (
    <div>
      {tokenFromURL && (
        <ForgotPassword
          tokenFromURL={tokenFromURL}
          open={open}
          type="reset"
          onClose={handleClose}
        />
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(ResetPassword)
