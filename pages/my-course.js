import React, { Fragment } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as AlertActions from '@actions/alertActions'

import withPage from '@containers/HigherOrderComponent/withPage'
// import Course from '@containers/MyCourse'

const Snackbar = dynamic(() => import('@components/Snackbar'), {
  ssr: false,
  loading: () => null,
})
const Course = dynamic(() => import('@containers/MyCourse'), {
  ssr: false,
  loading: () => null,
})

function MyCourse({ isAlert, alertActions }) {
  return (
    <Fragment>
      <Course />
      {isAlert.type === 'success' && (
        <Snackbar
          open={isAlert.open}
          text={isAlert.text}
          type={isAlert.type}
          onClose={alertActions.handleClose}
        />
      )}
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    isAlert: state.isAlert,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

const enhancer = compose(
  nextConnect(mapStateToProps, mapDispatchToProps),
  withPage({ restricted: true }),
)

export default enhancer(MyCourse)
