import React from 'react'
import dynamic from 'next/dynamic'

import withPage from '@containers/HigherOrderComponent/withPage'

const MyProfile = dynamic(() => import('@containers/MyProfile'), {
  ssr: false,
  loading: () => null,
})

function Profile() {
  return <MyProfile />
}

export default withPage({ restricted: true })(Profile)
