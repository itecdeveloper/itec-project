import React, { Fragment, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/courseActions'
import * as AlertActions from '@actions/alertActions'
import { useRouter } from 'next/router'
import Error from 'next/error'

import withPage from '@containers/HigherOrderComponent/withPage'
import CourseOverview from '@containers/CourseOverview'

const Snackbar = dynamic(() => import('@components/Snackbar'), {
  ssr: false,
  loading: () => null,
})

function getAuthDataFromCallbackURL(query) {
  const urlParams = new URLSearchParams(query)
  const id = urlParams.get('id') || false
  return {
    id,
  }
}

function Course({ courseDetails, actions, pageNotFound, isAlert, alertActions }) {
  const router = useRouter()
  useEffect(() => {
    const { id: idFromURL } = getAuthDataFromCallbackURL(window.location.search)
    actions.initCourseDetail(idFromURL)
    // console.log('idFromURL', idFromURL)
  }, [])
  // console.log('pageNotFound', pageNotFound)
  if (pageNotFound) {
    return <Error statusCode={404} />
    // return null
  }

  return (
    <Fragment>
      {Object.keys(courseDetails).length > 0 && <CourseOverview />}
      <Snackbar
        open={isAlert.open}
        text={isAlert.text}
        type={isAlert.type}
        onClose={alertActions.handleClose}
      />
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    pageNotFound: state.courses.isNotFoundCourses,
    courseDetails: state.courses.course,
    isAlert: state.isAlert,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
    alertActions: bindActionCreators(AlertActions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps), withPage())

export default enhancer(Course)
