import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'
import { Helmet } from 'react-helmet'

import { getStatic } from '../lib/static'

import { ServerStyleSheets } from '@material-ui/core/styles'
import theme from '../lib/theme'

// import '../public/static/css/fonts.css'

class CustomHead extends Head {
  render() {
    const { head, styles } = this.context._documentProps

    return (
      <head {...this.props}>
        {head}
        {this.getCssLinks()}
        {styles || null}
        {this.props.children}
      </head>
    )
  }
}

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const nextStyles = flush()
    const sheets = new ServerStyleSheets()
    const originalRenderPage = ctx.renderPage

    ctx.renderPage = () =>
      originalRenderPage({
        enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
      })

    const initialProps = await Document.getInitialProps(ctx)

    return {
      ...initialProps,
      // Styles fragment is rendered after the app and page rendering finish.
      styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
      helmet: Helmet.renderStatic(),
      nextStyles,
    }
  }

  render() {
    const { helmet } = this.props
    const htmlAttrs = helmet.htmlAttributes.toComponent()
    const bodyAttrs = helmet.bodyAttributes.toComponent()

    return (
      <html {...htmlAttrs}>
        <CustomHead>
          <title>Eduworld</title>
          <link
            rel="shortcut icon"
            href="https://itec-public-file.s3-ap-southeast-1.amazonaws.com/favicon/favicon.ico"
          />
          {/* <link rel="shortcut icon" href={`${getStatic('static/favicon.ico')}`} /> */}
          {helmet.title.toComponent()}
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="theme-color" content={theme.palette.primary.main} />
          {helmet.meta.toComponent()}
          <link rel="preload" href={`${getStatic('static/css/fonts.css')}`} as="style" />
          {helmet.link.toComponent()}
          {this.props.nextStyles}
          {helmet.script.toComponent()}
        </CustomHead>
        <body {...bodyAttrs}>
          {helmet.noscript.toComponent()}
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
