import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

import withPage from '@containers/HigherOrderComponent/withPage'

const useStyles = makeStyles((theme) => ({
  root: {
    // width: 375,
    fontSize: '14px',
    lineHeight: '160%',
    letterSpacing: '0.02em',
    // textTransform: 'capitalize',
    color: 'rgba(0, 0, 0, 0.6)',
    paddingTop: 24,
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
}))

function Support() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Container>
        <a
          style={{ textDecoration: 'none', color: 'rgba(0, 0, 0, 0.6)' }}
          href="mailto:study@eduworld.ac.th"
        >
          <p style={{ fontSize: 16 }}>Email: study@eduworld.ac.th</p>
        </a>
        <a style={{ textDecoration: 'none', color: 'rgba(0, 0, 0, 0.6)' }} href="tel:+66891399474">
          <p style={{ fontSize: 16 }}>Phone number: (+66) 89-139-8474</p>
        </a>
        <p style={{ fontSize: 16 }}>Line: @eduworld.online</p>
      </Container>
    </div>
  )
}

export default withPage()(Support)
