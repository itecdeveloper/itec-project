import React, { Fragment, useEffect } from 'react'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/orderActions'
import { useRouter } from 'next/router'
import { get } from 'lodash'

import withPage from '@containers/HigherOrderComponent/withPage'
import { Payment as PaymentComponent } from '@containers/Payment'

function getAuthDataFromCallbackURL(query) {
  const urlParams = new URLSearchParams(query)
  const token = urlParams.get('order') || false
  return {
    order_id: token,
  }
}

function Payment({ actions }) {
  const router = useRouter()
  useEffect(() => {
    const { order_id: orderIdFromURL } = getAuthDataFromCallbackURL(window.location.search)
    if (orderIdFromURL) {
      actions.initOrder(orderIdFromURL)
    }
    // console.log('orderIdFromURL', { orderIdFromURL })
  }, [])

  return (
    <Fragment>
      <PaymentComponent />
    </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(
  nextConnect(mapStateToProps, mapDispatchToProps),
  withPage({ restricted: true }),
)

export default enhancer(Payment)
