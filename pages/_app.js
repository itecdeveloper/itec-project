import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import App from 'next/app'
import Router from 'next/router'
import { CookiesProvider } from 'react-cookie'
import NProgress from 'nprogress'

import { ThemeProvider } from '@material-ui/core/styles'
import theme from '@lib/theme'
import { StylesProvider } from '@material-ui/core/styles'

import { withAuth } from '@containers/auth'

import * as font from '@lib/styles/font'
import GlobalStyle from '@lib/styles/GlobalStyle'

import '../public/static/css/nprogress.css'
import '../public/static/css/fonts.css'

class MyApp extends App {
  componentDidMount() {
    // const WebFont = require('webfontloader')
    // WebFont.load(font.config)

    Router.events.on('routeChangeStart', (url) => {
      // console.log('routeChangeStart', { url })
      NProgress.start()
      if (window.__NEXT_DATA__.props.isSSR === undefined) {
        window.__NEXT_DATA__.props.isSSR = false
      }
    })
    Router.events.on('routeChangeComplete', () => {
      // console.log('routeChangeComplete')
      NProgress.done()
    })
    Router.events.on('routeChangeError', () => {
      // console.log('routeChangeError')
      NProgress.done()
    })

    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }

  render() {
    const { Component, router } = this.props

    const children = (
      <Fragment>
        <GlobalStyle />
        {/* <Helmet titleTemplate={`%s - nextweb.js`} /> */}
        <StylesProvider injectFirst>
          <ThemeProvider theme={theme}>
            <Component {...this.props.pageProps} router={router} />
          </ThemeProvider>
        </StylesProvider>
      </Fragment>
    )

    if (process.browser) {
      return <CookiesProvider>{children}</CookiesProvider>
    }

    return children
  }
}

export default withAuth(MyApp)
// export default MyApp
