export const setAlert = (handleAlert) => {
  return {
    type: 'SET_ALERT',
    handleAlert,
  }
}
export const handleClose = (event, reason) => {
  if (reason === 'clickaway') {
    return {
      type: 'CLICK_AWAY',
    }
  }
  return {
    type: 'CLEAR_ALERT',
    open: false,
  }
}
