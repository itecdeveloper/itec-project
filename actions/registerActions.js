import { postAPIService, putAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import sha512 from '@lib/sha512'
import moment from 'moment'
import { AUTH_COOKIE_MAX_AGE, AUTH_COOKIE_ONE_YEAR_AGE } from '@config/constants'
import { setOpenModal } from './modalActions'
import { setAlert, handleClose } from './alertActions'

export const setPage = (page) => {
  return {
    type: 'SET_PAGE',
    page,
  }
}
export const setRegisterDetails = (details) => {
  return {
    type: 'SET_REGISTER_DETAILS',
    details,
  }
}
export const setAccessLoading = (isLoading) => {
  return {
    type: 'SET_ACCESS_ACCOUNT_LOADING',
    isLoading,
  }
}
export const setAccount = (payload) => {
  return {
    type: 'SET_ACCOUNT',
    payload,
  }
}
export const setForgotPasswordLoading = (isLoading) => {
  return {
    type: 'SET_FORGOT_PASSWORD_LOADING',
    isLoading,
  }
}
export const clearRegister = () => {
  return {
    type: 'CLEAR_REGISTER',
  }
}

export const auth = (data) => async (dispatch, getState) => {
  const url = getAPIRouter('AUTH')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    ...data,
    EMAIL: data.EMAIL.toLowerCase(),
    PASSWORD: sha512(data.PASSWORD),
  }
  dispatch(setAccessLoading(true))
  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    const payload = {
      ...res.message.data,
      exp: data.REMEMBER
        ? Math.floor(Date.now() / 1000) + AUTH_COOKIE_ONE_YEAR_AGE
        : Math.floor(Date.now() / 1000) + AUTH_COOKIE_MAX_AGE,
    }
    dispatch(setAccount(payload))
    dispatch(setOpenModal(false))
    dispatch(handleClose())
  } else {
    console.error('error', res)
    dispatch(
      setAlert({
        text: 'Email address or password is either incorrect or not registered to our system',
        type: 'error',
      }),
    )
  }
  dispatch(setAccessLoading(false))
}

export const rgisterAccount = () => async (dispatch, getState) => {
  const { registerDetails } = getState().register

  const url = getAPIRouter('ACCOUNT')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    ...registerDetails,
    EMAIL: registerDetails.EMAIL.toLowerCase(),
    PASSWORD: sha512(registerDetails.PASSWORD),
    DOB: moment(registerDetails.DOB, 'DD/MM/YYYY').valueOf(),
    AUTO_LOGIN: true,
  }
  // console.log('body', { body })
  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    // console.log('res', res)
    const payload = {
      ...res.message.data,
      exp: Math.floor(Date.now() / 1000) + AUTH_COOKIE_MAX_AGE,
    }
    // dispatch(setAlert({ text: 'Register success!', type: 'success' }))
    dispatch(setAccount(payload))
    dispatch(setOpenModal(false))
    dispatch(clearRegister())
    dispatch(handleClose())
  } else {
    console.error('error', res)
    dispatch(setAlert({ text: res.message.data, type: 'error' }))
  }
}

export const editRgisterAccount = (registerDetails) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('ACCOUNT')
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    ...registerDetails,
    DOB: moment(registerDetails.DOB, 'DD/MM/YYYY').valueOf(),
  }
  // console.log('body', { body })
  const res = await putAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    // console.log('res', res)
    const payload = {
      ...res.message.data,
      exp: Math.floor(Date.now() / 1000) + AUTH_COOKIE_MAX_AGE,
    }
    dispatch(setAccount(payload))
    dispatch(setAlert({ text: 'Edit success!', type: 'success' }))
  } else {
    console.error('error', res)
    dispatch(setAlert({ text: res.message.data, type: 'error' }))
  }
}

export const ValidateEmail = async (value) => {
  const url = getAPIRouter('SERVICE_EMAIL')
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    EMAIL: value.toLowerCase(),
  }

  const res = await postAPIService(url, header, JSON.stringify(body))
  if (res.status) {
    return true
  } else {
    return false
  }
}

export const requestPassword = (value) => async (dispatch, getState) => {
  const url = getAPIRouter('ACCOUNT_FORGOT')
  const finalUrl = url + '?type=EM_GET'
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    EMAIL: value.EMAIL.toLowerCase(),
    // HOST: 'http://localhost:3000/reset-password',
    HOST: 'https://www.eduworld.ac.th/reset-password',
  }

  const res = await postAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    // console.log('requestPassword', res)
    return res
  } else {
    console.error('error requestPassword', res)
    return res
  }
}

export const resetPassword = (value) => async (dispatch, getState) => {
  const url = getAPIRouter('ACCOUNT_FORGOT')
  const finalUrl = url + '?type=EM_SET'
  const header = {
    'Content-Type': 'application/json',
  }
  const body = {
    token: value.tokenFromURL,
    PASSWORD: sha512(value.PASSWORD),
  }

  const res = await postAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    // console.log('resetPassword', res)
    return res
  } else {
    console.error('error resetPassword', res)
    dispatch(setAlert({ text: res.message.data, type: 'error' }))
    return res
  }
}
