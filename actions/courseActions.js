import { getAPIService, putAPIService, delAPIService } from '@services/apiServices'
import { getAPIRouter } from '@config/APIConfig'
import {
  formatCalendarTeacher,
  formatCalendar,
  formatMyCourse,
  rescheduleTimeStem,
  bookandrescheduleflow,
  fomatOurCourse,
  fomatOurInstitute,
} from '@util/myCourseFunc'
import moment from 'moment'
import { getAdminUser } from '@config/constants'
import { setViewClassOpenModal } from './modalActions'

export const setInstitute = (institute) => {
  return {
    type: 'SET_INSTITUTE',
    institute,
  }
}

export const setInstituteDetail = (instituteDetail) => {
  return {
    type: 'SET_INSTITUTE_DETAIL',
    instituteDetail,
  }
}

export const setAllCourses = (allCourses) => {
  return {
    type: 'SET_ALL_COURSES',
    allCourses,
  }
}

export const setCourse = (course) => {
  return {
    type: 'SET_COURSE',
    course,
  }
}

export const setNotFoundCourse = (isNotFoundCourses) => {
  return {
    type: 'SET_NOT_FOUND_COURS',
    isNotFoundCourses,
  }
}

export const setCourseLoading = (isLoading) => {
  return {
    type: 'SET_COURSE_LOADING',
    isLoading,
  }
}

export const setCalendar = (calendar) => {
  return {
    type: 'SET_CALENDAR',
    calendar,
  }
}

export const setSelectCalendar = (selectCalendar) => {
  return {
    type: 'SET_SELECT_CALENDAR',
    selectCalendar,
  }
}

export const setMyCourse = (myCourses) => {
  return {
    type: 'SET_MY_COURSE',
    myCourses,
  }
}

export const setMyCourseWaitPayment = (coursesWaitPayment) => {
  return {
    type: 'SET_MY_COURSE_WAIT_PAYMENT',
    coursesWaitPayment,
  }
}

export const setSessionDetail = (sessionDetail) => {
  return {
    type: 'SET_SESSION_DETAIL',
    sessionDetail,
  }
}

export const setViewSession = (viewSession) => {
  return {
    type: 'SET_VIEW_SESSION',
    viewSession,
  }
}

export const setAccountTeacherCourse = (accountsTeacher) => {
  return {
    type: 'SET_ACCOUNT_TEACHER_COURSE',
    accountsTeacher,
  }
}

export const setAccountStudentCourse = (accountsStudent) => {
  return {
    type: 'SET_ACCOUNT_STUDENT_COURSE',
    accountsStudent,
  }
}

export const setMyCourseActionLoading = (isLoading) => {
  return {
    type: 'SET_MY_COURSE_LOADING',
    isLoading,
  }
}

export const setOpenBookingModal = (open) => {
  return {
    type: 'SET_BOOKING_OPEN_MODAL',
    open,
  }
}

export const setIntituteBannerLoading = (isLoading) => {
  return {
    type: 'SET_INSTITUTE_BANNER_LOADING',
    isLoading,
  }
}

export const initInstitute = () => async (dispatch, getState) => {
  const url = getAPIRouter('GET_INSTITUTE')
  const res = await getAPIService(url, {})
  if (res.status) {
    const items = res.message.data.Items
    dispatch(setInstitute(fomatOurInstitute(items)))
    // console.log('res', res)
  } else {
    console.error('error', res)
  }
}

export const initInstituteDetail = (slugFromURL) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_INSTITUTE')
  const finalUrl = url + `?filter=SLUG&qr=${slugFromURL}`
  dispatch(setIntituteBannerLoading(true))
  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data.Items[0]
    dispatch(setInstituteDetail(items))
    // console.log('res', res)
  } else {
    console.error('error', res)
  }
  dispatch(setIntituteBannerLoading(false))
}

export const initCourses = (slugFromURL) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_ALL_COURSES')
  const finalUrl = url + `?filter=SLUG&qr=${slugFromURL}`
  dispatch(setCourseLoading(true))
  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    dispatch(setAllCourses(fomatOurCourse(items)))
    // console.log('res', items)
  } else {
    console.error('error', res)
  }
  dispatch(setCourseLoading(false))
}

export const initCourseDetail = (id) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_COURSE_DETAILS')
  const finalUrl = url.replace('param', id)
  dispatch(setNotFoundCourse(false))
  dispatch(setCourseLoading(true))
  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    // console.log('initCourseDetail', items)
    dispatch(setCourse(items))
  } else {
    dispatch(setNotFoundCourse(true))
    console.error('error', res)
  }
  dispatch(setCourseLoading(false))
}

export const initCourseStudentOrTeacher = () => async (dispatch, getState) => {
  const account = getState().account
  let url = ''
  console.log('initCourseStudentOrTeacher')

  if (getAdminUser(account.profile.TYPE)) {
    url = getAPIRouter('GET_TEACHER')
  } else {
    url = getAPIRouter('GET_STUDENT')
  }

  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const res = await getAPIService(url, header)
  if (res.status) {
    const items = res.message.data
    const uniqueData = [...new Set(items.DETAIL.map((a) => a.COURSE_ID))]

    const awaitData = await Promise.all(uniqueData.map((data) => dispatch(initGetCourseById(data))))

    if (getAdminUser(account.profile.TYPE)) {
      const sessionForThacher = await Promise.all(
        items.DETAIL.map((data) => dispatch(initGetOrderByTeacher(data))),
      )
      const newItems = {
        ...items,
        SESSIONS: sessionForThacher.reduce((arr, structure) => {
          arr = [...arr, ...structure.SESSIONS]
          return arr
        }, []),
      }
      const fomatData = formatMyCourse(
        newItems,
        awaitData.filter((i) => Object.keys(i).length > 0),
      )
      await dispatch(setMyCourseWaitPayment(fomatData.orderNotSuccess || []))
      await dispatch(setMyCourse(fomatData.orderSuccess || []))
      const calendar = await formatCalendarTeacher(fomatData.orderSuccess || [])
      dispatch(setCalendar(calendar))
      dispatch(selectCalendar(moment().format('D MMM YYYY')))

      const uniqueStudentId = [...new Set(items.DETAIL.map((a) => a.STUDENT_ID))]
      Promise.all(uniqueStudentId.map((data) => dispatch(getAccountForSession(data))))
      const uniqueTeacherId = [...new Set(items.DETAIL.map((a) => a.TEACHER_ID))]
      Promise.all(uniqueTeacherId.map((data) => dispatch(getAccountForSession(data))))
      // console.log('sessionForThacher', { sessionForThacher, items, awaitData, newItems })
    } else {
      const fomatData = formatMyCourse(
        items,
        awaitData.filter((i) => Object.keys(i).length > 0),
      )
      await dispatch(setMyCourseWaitPayment(fomatData.orderNotSuccess || []))
      await dispatch(setMyCourse(fomatData.orderSuccess || []))
      const calendar = await formatCalendar(fomatData.orderSuccess || [])
      dispatch(setCalendar(calendar))
      dispatch(selectCalendar(moment().format('D MMM YYYY')))

      const uniqueStudentId = [...new Set(items.DETAIL.map((a) => a.STUDENT_ID))]
      Promise.all(uniqueStudentId.map((data) => dispatch(getAccountForSession(data))))
      const uniqueTeacherId = [...new Set(items.DETAIL.map((a) => a.TEACHER_ID))]
      Promise.all(uniqueTeacherId.map((data) => dispatch(getAccountForSession(data))))
      // console.log('res initCourseStudent', { uniqueStudentId, uniqueTeacherId })
    }
  } else {
    console.error('error initCourseStudent', res)
  }
}

export const initGetOrderByTeacher = (data) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', data.ORDER_ID)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }

  const res = await getAPIService(finalUrl, header)
  if (res.status) {
    const items = res.message.data
    // console.log('res initGetOrderByTeacher', res)
    return items
  } else {
    console.error('error initGetOrderByTeacher', res)
    return []
  }
}

export const initGetCourseById = (course_id) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_COURSE_DETAILS')
  const finalUrl = url.replace('param', course_id)

  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    // console.log('res initLevel', res)
    return items
  } else {
    console.error('error initGetCourseById', res)
    return {}
  }
}

export const initLevel = (LEVEL_ID) => async (dispatch, getState) => {
  const url = getAPIRouter('GET_LEVEL')
  const finalUrl = url.replace('param', LEVEL_ID)

  const res = await getAPIService(finalUrl, {})
  if (res.status) {
    const items = res.message.data
    // console.log('res initLevel', res)
    return items
  } else {
    console.error('error initLevel', res)
    return {}
  }
}

export const selectCalendar = (value) => async (dispatch, getState) => {
  const { calendars } = getState().myCourse
  dispatch(setSelectCalendar(calendars[value]))
}

export const initSession = (index) => async (dispatch, getState) => {
  const { myCourses } = getState().myCourse
  await dispatch(setSessionDetail(myCourses[index]))
}

export const viewSession = (orderId, sessionId) => async (dispatch, getState) => {
  const { myCourses } = getState().myCourse
  const course = myCourses.find((course) => course.ORDER_ID === orderId)
  const session = course.SESSIONS.find((session) => session.SESSION_ID === sessionId)
  const item = {
    ...session,
    COURSE_DETAIL: course.COURSE_DETAIL,
  }
  await dispatch(setViewSession(item))
  dispatch(setViewClassOpenModal(true))
}

export const statusSession = (data) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('UPDATE_SESSION')
  const orderUrl = url.replace('param1', data.orderId)
  const finalUrl = orderUrl.replace('param2', data.sessionId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    S_STATUS: data.value === 'completed' ? 'COMPLETE' : 'INCOMPLETE',
  }
  dispatch(setMyCourseActionLoading(true))

  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    dispatch(initCourseStudentOrTeacher())
    // console.log('statusSession', res)
  } else {
    console.error('error statusSession', res)
  }
  dispatch(setMyCourseActionLoading(false))
}

export const rescheduleSession = (data) => async (dispatch, getState) => {
  const account = getState().account
  const viewSession = getState().myCourse.viewSession

  const url = getAPIRouter('UPDATE_SESSION')
  const orderUrl = url.replace('param1', data.orderId)
  const finalUrl = orderUrl.replace('param2', data.sessionId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const class_time = `${data.time_hour.value}:${data.time_min.value}`
  const NEW_START_TIME = rescheduleTimeStem(data.date, class_time)
  const body = {
    NEW_START_TIME,
    STATUS_NEW_START_TIME: 'req',
  }
  // console.log('NEW_START_TIME', { NEW_START_TIME, viewSession, data })

  if (
    bookandrescheduleflow(
      NEW_START_TIME,
      viewSession.RCREATED_AT,
      viewSession.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME,
      viewSession.COURSE_DETAIL.DETAIL.CREATE_TICKET_TIME_LIMIT,
    )
  ) {
    const res = await putAPIService(finalUrl, header, JSON.stringify(body))
    if (res.status) {
      // console.log('rescheduleSession', res)
      // dispatch(setViewClassOpenModal(false))
      return res
    } else {
      console.error('error rescheduleSession', res)
      return res
    }
  } else {
    return {
      status: false,
      data: NEW_START_TIME,
    }
  }
}

export const editSession = (data) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('UPDATE_SESSION')
  const orderUrl = url.replace('param1', data.orderId)
  const finalUrl = orderUrl.replace('param2', data.sessionId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    LINKS: data.links,
    NOTE: data.note,
  }

  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    // console.log('editSession', res)
    dispatch(initCourseStudentOrTeacher())
    return res
  } else {
    console.error('error editSession', res)
    return res
  }
}

export const getAccountForSession = (accountId) => async (dispatch, getState) => {
  const account = getState().account

  const url = getAPIRouter('ACCOUNT_ID')
  const finalUrl = url.replace('param', accountId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  // console.log('getAccountForSession', accountId)

  if (accountId !== '-') {
    const res = await getAPIService(finalUrl, header)
    if (res.status) {
      const accountTeacherCourse = getState().myCourse.accountsTeacher
      const accountStudentCourse = getState().myCourse.accountsStudent
      const items = res.message.data.Items[0]
      if (getAdminUser(items.TYPE)) {
        dispatch(setAccountTeacherCourse([...accountTeacherCourse, items]))
      } else {
        dispatch(setAccountStudentCourse([...accountStudentCourse, items]))
      }
      // console.log('getAccountForSession', items)
    } else {
      console.error('error getAccountForSession', res)
    }
  }
}

export const cancleOrder = (data, items) => async (dispatch, getState) => {
  const account = getState().account
  const { coursesWaitPayment } = getState().myCourse

  const url = getAPIRouter('GET_ORDER')
  const finalUrl = url.replace('param', data.ORDER_ID)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    STATUS: 'INACTIVE',
  }

  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    const resData = res.message.data
    const filterItems = coursesWaitPayment.find((i) => i.ORDER_ID === resData.ORDER_ID)
    const filterTempOrder = coursesWaitPayment.filter((i) => i.ORDER_ID !== resData.ORDER_ID)
    const result = {
      ...filterItems,
      STATUS: resData.STATUS,
    }
    // console.log('cancleOrder', {
    //   res,
    //   items,
    //   filterItems,
    //   filterTempOrder,
    //   result,
    //   coursesWaitPayment,
    // })
    await dispatch(setMyCourseWaitPayment([...filterTempOrder, result]))
  } else {
    console.error('error cancleOrder', res)
  }
}

export const CancelBooking = (data) => async (dispatch, getState) => {
  const account = getState().account
  const viewSession = getState().myCourse.viewSession

  const url = getAPIRouter('DELETE_SESSION')
  const orderUrl = url.replace('param1', data.orderId)
  const finalUrl = orderUrl.replace('param2', data.sessionId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    SESSIONS: [
      {
        ORDER_ID: data.orderId,
        SSID: data.sessionId,
      },
    ],
  }
  // console.log('NEW_START_TIME', { NEW_START_TIME, viewSession, data })

  const res = await delAPIService(orderUrl, header, JSON.stringify(body))
  if (res.status) {
    // console.log('CancelBooking', res)
    dispatch(initCourseStudentOrTeacher())
    return res
  } else {
    console.error('error CancelBooking', res)
    return res
  }
}

export const CancelRequest = (data) => async (dispatch, getState) => {
  const account = getState().account
  const viewSession = getState().myCourse.viewSession

  const url = getAPIRouter('UPDATE_SESSION')
  const orderUrl = url.replace('param1', data.orderId)
  const finalUrl = orderUrl.replace('param2', data.sessionId)
  const header = {
    'Content-Type': 'application/json',
    Authorization: account.token,
  }
  const body = {
    STATUS_NEW_START_TIME: 'reject',
  }
  // console.log('NEW_START_TIME', { NEW_START_TIME, viewSession, data })

  const res = await putAPIService(finalUrl, header, JSON.stringify(body))
  if (res.status) {
    // console.log('CancelRequest', res)
    dispatch(initCourseStudentOrTeacher())
    return res
  } else {
    console.error('error CancelRequest', res)
    return res
  }
}
