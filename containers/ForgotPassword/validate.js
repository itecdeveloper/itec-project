import * as yup from 'yup'
import { ValidateEmail } from '@actions/registerActions'

export const schemaRequest = () =>
  yup.object().shape({
    EMAIL: yup
      .string()
      .test(
        'EMAIL',
        'This email has not been registered',
        async (value) => (await ValidateEmail(value)) === false,
      )
      .email('Invalid Email')
      .required('this field is a required'),
  })

export const schemaNewpassword = ({ PASSWORD }) =>
  yup.object().shape({
    PASSWORD: yup.string().min(8).required('this field is a required'),
    RE_PASSWORD: yup
      .string()
      .oneOf([PASSWORD], 'confirm password must be one of the password')
      .required('this field is a required'),
  })

export const schemaValidate = async (data, schema) => {
  // console.log('schemaValidate', { data, schema })
  return await schema(data)
    .validate(data, { abortEarly: false })
    .then((res) => {
      // console.log('res', { res })
      return { values: res, errors: {} }
    })
    .catch((err) => {
      // console.log('err', err)
      const errors = err.inner.reduce((allErrors, currentError) => {
        return {
          ...allErrors,
          [currentError.path]: {
            type: currentError.type ?? 'validation',
            message: currentError.message,
          },
        }
      }, {})
      return {
        values: {},
        errors,
      }
    })
}
