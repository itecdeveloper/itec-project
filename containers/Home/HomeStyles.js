import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 345,
    boxShadow: 'none',
    borderRadius: 0,
    // display: 'flex',
    height: '100%',
  },
  media: {
    height: 124,
    borderRadius: 5,
    width: 248,
    flex: '1 0 auto',
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  cardActionArea: {
    display: 'flex',
    flexDirection: 'row',
    paddingLeft: 24,
    paddingTop: 24,
    paddingBottom: 24,
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      paddingRight: 24,
    },
  },
  content: {
    // flex: '1 0 auto',
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  typographyTitle: {
    fontWeight: 500,
    fontSize: '24px',
    lineHeight: '28px',
    color: '#333333',
    textTransform: 'capitalize',
  },
  typographyDetail: {
    fontSize: 14,
    lineHeight: '20px',
    color: '#808080',
  },
  cardFirst: {
    borderTopLeftRadius: 10,
  },
  cardSecond: {
    borderTopRightRadius: 10,
  },
  cardThire: {
    borderBottomLeftRadius: 10,
  },
  cardForu: {
    borderBottomRightRadius: 10,
  },
}))

const HomeStyles = makeStyles((theme) => ({
  root: {
    background: '#E5E8ED',
    paddingBottom: 56,
  },
  cardContainer: {
    background: '#FFFFFF',
    boxShadow: '0px 1px 12px rgba(0, 0, 0, 0.08)',
    borderRadius: '10px',
    marginTop: 56,
    // padding: '25px 0px',
    // paddingBottom: 15,
  },
  gridContainer: {
    // '& :nth-child(odd)': {
    //   border: '1px solid red',
    // },
  },
  gridItem: {
    '&:nth-child(odd)': {
      borderRight: '1px solid #F1EFEF',
      borderBottom: '1px solid #F1EFEF',
    },
    '&:nth-child(even)': {
      borderBottom: '1px solid #F1EFEF',
      // borderLeft: '0.5px solid #F1EFEF',
    },
  },
}))

export default HomeStyles
