import React from 'react'
import dynamic from 'next/dynamic'

import Navigation from '@containers/Navigation'
import Footer from '@containers/Footer'

const FlowAccessAccount = dynamic(() => import('@containers/FlowAccessAccount'), {
  ssr: false,
  loading: () => null,
})

function MainLayout({ children }) {
  return (
    <div className="main-container">
      <Navigation />
      <FlowAccessAccount />
      <main className="main">{children}</main>
      <Footer />
    </div>
  )
}

export default MainLayout
