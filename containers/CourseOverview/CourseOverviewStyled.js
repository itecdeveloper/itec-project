import { makeStyles } from '@material-ui/core/styles'

const CourseOverviewStyles = makeStyles((theme) => ({
  root: {},
  breadcrumbRoot: {
    padding: '30px 32px',
  },
  otherCourse: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 478,
    backgroundColor: '#F6F6F6',
  },
}))

export default CourseOverviewStyles
