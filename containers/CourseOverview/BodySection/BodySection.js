import React, { Fragment, useState } from 'react'
import dynamic from 'next/dynamic'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'
import Icon from '@components/Icon'

import BodySectionStyled, {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from './BodySectionStyled'

const BackdropLoading = dynamic(() => import('@components/BackdropLoading'), {
  ssr: false,
  loading: () => null,
})

function BodySection({ details, levels, orderIsLoading }) {
  const [expanded, setExpanded] = useState(null)

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false)
  }
  const classes = BodySectionStyled()

  return (
    <div className={classes.root}>
      <div className={classes.sectionRoot}>
        <Typography className="title">About this course</Typography>
        <Typography style={{ whiteSpace: 'pre-wrap' }} className="sub-title" variant="p">
          {details.DETAIL}
        </Typography>
      </div>
      {orderIsLoading && <BackdropLoading open={orderIsLoading} />}
    </div>
  )

  // return (
  //   <div className={classes.root}>
  //     <div className={classes.sectionRoot}>
  //       {levels &&
  //         levels.map((level, index) => (
  //           <ExpansionPanel
  //             key={index}
  //             square
  //             expanded={expanded === index}
  //             onChange={handleChange(index)}
  //           >
  //             <ExpansionPanelSummary
  //               aria-controls={index}
  //               id={index}
  //               expandIcon={
  //                 <Icon style={{ color: '#ED1B24', fontSize: '14px', overflow: 'unset' }}>
  //                   arrowDownward
  //                 </Icon>
  //               }
  //             >
  //               <Grid container wrap="nowrap" spacing={2}>
  //                 <Grid item>
  //                   <Typography className={classes.typographyLevel}>{level.LEVEL}</Typography>
  //                 </Grid>
  //                 {expanded !== index && (
  //                   <Grid item xs zeroMinWidth>
  //                     <Typography className={classes.typographyLevelSecond} noWrap>
  //                       {level.DETAIL}
  //                     </Typography>
  //                   </Grid>
  //                 )}
  //               </Grid>
  //             </ExpansionPanelSummary>
  //             <ExpansionPanelDetails>
  //               <Typography>{level.DETAIL}</Typography>
  //             </ExpansionPanelDetails>
  //           </ExpansionPanel>
  //         ))}
  //     </div>
  //     {orderIsLoading && <BackdropLoading open={orderIsLoading} />}
  //   </div>
  // )
}

export default BodySection
