import { makeStyles } from '@material-ui/core/styles'

const HeaderStyles = makeStyles((theme) => ({
  root: {},
  titleRoot: {
    // borderLeft: `8px solid ${theme.palette.primary.main}`,
    // paddingLeft: 10,
    fontSize: '28px',
    fontWeight: 500,
    lineHeight: '36px',
    letterSpacing: '0em',
    color: '#333333',
  },
  detailsRoot: {
    marginTop: 20,
  },
  details: {
    fontSize: '1.2857rem',
    fontWeight: 300,
    lineHeight: '163.2%',
    color: '#7A7A7A',
  },
  imageRoot: {
    width: '100%',
    // height: '247px',
    height: '100%',
    margin: 'auto',
  },
  image: {
    color: 'transparent',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    textAlign: 'center',
    textIndent: '10000px',
    borderRadius: '5px',
  },
  gridContainer: {
    padding: '6px 0',
  },
  firstText: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  lastText: {
    color: 'rgba(0, 0, 0, 0.8)',
    fontSize: '16px',
    lineHeight: '20px',
  },
  bottonRoot: {
    marginTop: 16,
  },
  card: {
    background: '#FFFFFF',
    boxShadow: '0px 1px 12px rgba(0, 0, 0, 0.08)',
    borderRadius: '10px',
    padding: 24,
  },
  note: {
    fontSize: '13px',
    lineHeight: '20px',
    color: '#808080',
    marginTop: '16px',
    textAlign: 'center',
  },
}))

export default HeaderStyles
