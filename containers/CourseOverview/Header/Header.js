import React, { Fragment } from 'react'
import {
  toNumber,
  displayTime,
  getAge,
  ageRang,
  ageDuration,
  displayAgeRang,
  splitCourseName,
} from '@util/dataFormate'
import { getStudentUser } from '@config/constants'
import { useMember } from '@containers/auth'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Button from '@components/Button'

import HeaderStyled from './HeaderStyled'

function Header({ details, levels, ages, bookingActions, modalActions, items }) {
  const {
    COURSE_NAME,
    DETAIL,
    COURSE_IMG,
    PRICE,
    SESSION_QUANTITY,
    SESSION_TIME,
    AGE,
    INSTITUTE,
  } = details
  const { isAuthenticated, profile } = useMember()
  const classes = HeaderStyled()
  const handleOpenBook = () => {
    if (modalActions) {
      isAuthenticated
        ? bookingActions.initBookingDetail(ages, getAge(profile.DOB))
        : modalActions.setOpenModal(true)
      // isAuthenticated ? setOpenBook((prev) => !prev) : modalActions.setOpenModal(true)
    }
  }
  // console.log('Header', { details, items })

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={6}>
          <div className={classes.card}>
            <div className={classes.titleRoot}>
              <Typography className={classes.titleRoot} variant="h3">
                {splitCourseName(COURSE_NAME)[0]}
              </Typography>
              {splitCourseName(COURSE_NAME)[1] && (
                <Typography className={classes.titleRoot} variant="h3">
                  {splitCourseName(COURSE_NAME)[1]}
                </Typography>
              )}
            </div>
            <div className={classes.detailsRoot}>
              {/* <Typography className={classes.details}>{DETAIL}</Typography> */}
              <Grid container className={classes.gridContainer}>
                <Grid item xs={12} sm={3}>
                  <span className={classes.firstText}>Course By </span>
                </Grid>
                <Grid item xs={12} sm={9}>
                  <span
                    onClick={() => window.history.back()}
                    style={{ color: '#ED1B24', cursor: 'pointer' }}
                    className={classes.lastText}
                  >
                    {INSTITUTE}
                  </span>
                </Grid>
              </Grid>
              <Grid container className={classes.gridContainer}>
                <Grid item xs={12} sm={3}>
                  <span className={classes.firstText}>Duration </span>
                </Grid>
                <Grid item xs={12} sm={9}>
                  {['Inlingua', 'New'].includes(INSTITUTE) ? (
                    <span className={classes.lastText}>
                      {`${SESSION_QUANTITY} periods (${ageDuration(SESSION_TIME)})`}
                    </span>
                  ) : (
                    <span className={classes.lastText}>
                      {displayTime(SESSION_TIME * SESSION_QUANTITY)}
                      {` • ${SESSION_QUANTITY} sessions (${ageDuration(SESSION_TIME)})`}
                    </span>
                  )}
                </Grid>
              </Grid>
              <Grid container className={classes.gridContainer}>
                <Grid item xs={12} sm={3}>
                  <span className={classes.firstText}>Age </span>
                </Grid>
                <Grid item xs={12} sm={9}>
                  <span className={classes.lastText}>{displayAgeRang(details.AGE)}</span>
                </Grid>
              </Grid>
              <Grid container className={classes.gridContainer}>
                <Grid item xs={12} sm={3}>
                  <span className={classes.firstText}>Price </span>
                </Grid>
                <Grid item xs={12} sm={9}>
                  <span className={classes.lastText}>{`${toNumber(PRICE, '0,0')} THB`}</span>
                </Grid>
              </Grid>
            </div>
            {levels.length > 0 &&
              (isAuthenticated ? (
                getStudentUser(profile.TYPE) &&
                (ageRang(ages, getAge(profile.DOB)) ? (
                  <div className={classes.bottonRoot}>
                    <Button
                      style={{ minHeight: 48 }}
                      color="primary"
                      size="small"
                      fullWidth
                      onClick={handleOpenBook}
                    >
                      Buy Course
                    </Button>
                  </div>
                ) : (
                  <div className={classes.bottonRoot}>
                    <Button
                      style={{ minHeight: 48 }}
                      color="primary"
                      size="small"
                      fullWidth
                      disabled
                    >
                      Buy Course
                    </Button>
                  </div>
                ))
              ) : (
                <div className={classes.bottonRoot}>
                  <Button
                    style={{ minHeight: 48 }}
                    color="primary"
                    size="small"
                    fullWidth
                    onClick={handleOpenBook}
                  >
                    Buy Course
                  </Button>
                </div>
              ))}
            <div className={classes.note}>
              Class date can be booked after the payment is complete
            </div>
          </div>
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <div className={classes.imageRoot}>
            <img className={classes.image} src={COURSE_IMG} alt="" />
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Header
