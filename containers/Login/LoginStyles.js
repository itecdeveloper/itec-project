import { makeStyles } from '@material-ui/core/styles'

const LoginStyles = makeStyles((theme) => ({
  root: {
    width: '854px',
    height: '642px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      // height: '100%',
    },
  },
  titleRoot: {
    marginBottom: '24px',
    width: '360px',
    textAlign: 'center',
  },
  title: {
    fontSize: '2.5714rem',
    lineHeight: '44px',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  inputRoot: {
    marginBottom: 16,
  },
  inputRootDisplayFlex: {
    display: 'flex',
    '& .remember': {
      flexGrow: 1,
    },
    '& .forgot': {
      fontWeight: '500',
      fontSize: 14,
      lineHeight: '20px',
      color: theme.palette.primary.main,
      cursor: 'pointer',
    },
  },
  buttonRoot: {
    marginBottom: 24,
    position: 'relative',
  },
  buttonProgress: {
    color: theme.palette.primary.main,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  accountRoot: {
    textAlign: 'center',
    cursor: 'pointer',
  },
  account: {
    color: theme.palette.primary.main,
    fontSize: '18px',
    fontWeight: 600,
    lineHeight: '28px',
  },
  formControlLabel: {
    margin: 0,
  },
  radioRoot: {
    color: '#CCCCCC',
    padding: 0,
    marginRight: 8,
    '&$checked': {
      color: theme.palette.primary.main,
    },
  },
  checked: { color: theme.palette.primary.main },
  labelCheck: {
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '0.02em',
    color: 'rgba(0, 0, 0, 0.56)',
  },
  alertRoot: {
    marginBottom: '24px',
    width: '360px',
  },
  alert: {
    color: theme.palette.primary.main,
    backgroundColor: '#FDE4E5',
    boxShadow: 'none',
    borderRadius: '5px',
  },
  alertIcon: {
    alignItems: 'center',
  },
}))

export default LoginStyles
