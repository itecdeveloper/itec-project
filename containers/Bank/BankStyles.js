import { makeStyles } from '@material-ui/core/styles'

const BankStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // height: '100vh',
    backgroundColor: '#F8F7F7',
  },
  paper: {
    width: 375,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: 21,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 44,
      paddingRight: 44,
    },
  },
  BankSlipRoot: {
    textAlign: 'center',
    marginTop: 32,
    marginBottom: 8,
  },
  BankSlipHeader: {
    background: '#F8F7F7',
    borderRadius: '5px',
    fontSize: 13,
    lineHeight: '16px',
    color: theme.palette.primary.main,
    padding: '8px 28px',
    marginBottom: 23,
  },
  BankSlipAmount: {
    lineHeight: '30px',
    color: '#4D4D4D',
    marginBottom: 8,
    '& .amount': {
      fontSize: 14,
      marginRight: 5,
    },
    '& .price': {
      fontSize: 24,
      fontWeight: 'bold',
    },
  },
  BankSlipQrcode: {
    width: 220,
    height: 219,
    backgroundColor: '#cccccc',
    marginBottom: 12,
    marginLeft: 'auto',
    marginRight: 'auto',
    objectFit: 'contain',
    objectPosition: 'center',
  },
  BankSlipText: {
    fontWeight: 500,
    fontSize: 14,
    lineHeight: '20px',
    textTransform: 'capitalize',
    color: '#4D4D4D',
  },
  BankSlipNumber: {
    fontWeight: 600,
    fontSize: 32,
    lineHeight: '40px',
    textTransform: 'lowercase',
    fontFeatureSettings: `'tnum' on, 'lnum' on`,
    color: '#4D4D4D',
    marginBottom: 16,
  },
  BankSlipImage: {},
  BankDetailRoot: {
    marginBottom: 56,
  },
  BankDetailTitle: {
    fontSize: 14,
    lineHeight: '20px',
    color: '#4D4D4D',
    marginBottom: 8,
    '& .span1': {
      fontWeight: 600,
    },
  },
  BankDetailList: {
    '& ul': {
      paddingLeft: 16,
      fontSize: 14,
      lineHeight: '20px',
      color: '#4D4D4D',
    },
  },
  BankDetailContact: {
    fontSize: 14,
    lineHeight: '20px',
    color: '#4D4D4D',
  },
}))

export default BankStyles
