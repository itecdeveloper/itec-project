import React, { useState } from 'react'
import { getStatic } from '@lib/static'

import IconButton from '@material-ui/core/IconButton'

import Link from '@components/Link'
import Icon from '@components/Icon'
import Terms from '@components/Terms'

import FooterStyles from './FooterStyled'

function Footer() {
  const [term, setTerm] = useState(false)
  const classes = FooterStyles()

  return (
    <footer className={classes.root}>
      <div className={classes.footerGroup}>
        <div>
          <Link route="home" passHref>
            <img
              // src={`${getStatic('static/images/Eduworld.png')}`}
              src="https://itec-public-file.s3-ap-southeast-1.amazonaws.com/ivcon.png"
              alt="image"
              style={{ width: 48 }}
            />
          </Link>
        </div>
        <div className={classes.copyrightRoot}>
          <span>&#169; 2010 - 2020</span>
          <span className={classes.privacy} onClick={() => setTerm((prev) => !prev)}>
            Privacy - Terms
          </span>
        </div>
        {/* <div>
          <IconButton
            component="a"
            href="https://www.facebook.com/eduworld.bangkok"
            target="_blank"
            aria-label="facebook"
            className={classes.iconButton}
          >
            <Icon>facebook</Icon>
          </IconButton>
          <IconButton
            component="a"
            href="https://twitter.com/eduworldbangkok"
            target="_blank"
            aria-label="twitter"
            className={classes.iconButton}
          >
            <Icon>twitter</Icon>
          </IconButton>
          <IconButton aria-label="delete" className={classes.iconButton}>
            <Icon>youtube</Icon>
          </IconButton>
        </div> */}
      </div>
      <Terms open={term} onClose={() => setTerm((prev) => !prev)} />
    </footer>
  )
}

export default Footer
