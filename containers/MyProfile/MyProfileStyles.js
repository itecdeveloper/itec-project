import { makeStyles } from '@material-ui/core/styles'

const MyProfileStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // height: '100vh',
    backgroundColor: '#F8F7F7',
  },
  paper: {
    width: 375,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: 21,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 44,
      paddingRight: 44,
    },
  },
  formRoot: {
    width: 702,
    marginTop: 24,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  personal: {
    fontWeight: 600,
    fontSize: '16px',
    lineHeight: '24px',
    color: 'rgba(0, 0, 0, 0.7)',
    marginBottom: 8,
  },
  inputRoot: {
    marginBottom: 16,
  },
  buttonRoot: {
    marginBottom: 24,
    textAlign: 'center',
  },
}))

export default MyProfileStyles
