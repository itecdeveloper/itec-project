import React, { Fragment, useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators, compose } from 'redux'
import { nextConnect } from '@redux/store'
import * as Actions from '@actions/courseActions'
import moment from 'moment'
import { getStudentUser, getAdminUser } from '@config/constants'

import Container from '@material-ui/core/Container'
import Alert from '@material-ui/lab/Alert'
import { Grid, Typography } from '@material-ui/core'

import Icon from '@components/Icon'
import Calendar from '@components/Calendar'

import { useMember } from '@containers/auth'

import ScheduleDetails from './ScheduleDetails'
import MyCoursesStyles from './MyCoursesStyled'
import Panding from './Panding'

const Student = dynamic(() => import('./Student'), {
  ssr: false,
  loading: () => null,
})
const Teacher = dynamic(() => import('./Teacher'), {
  ssr: false,
  loading: () => null,
})
const BackdropLoading = dynamic(() => import('@components/BackdropLoading'), {
  ssr: false,
  loading: () => null,
})

function MyCourses({ calendarDetails, actionIsLoading, actions, openBooking }) {
  const { profile } = useMember()
  const [startDate, setStartDate] = useState(null)

  const {
    calendars,
    selectCalendar,
    coursesWaitPayment,
    myCourses,
    sessionDetail,
    accountsTeacher,
    accountsStudent,
  } = calendarDetails
  const classes = MyCoursesStyles()

  const handleChangeDate = (value) => {
    actions.selectCalendar(value)
    setStartDate(value)
  }

  useEffect(() => {
    actions.initCourseStudentOrTeacher()
    setStartDate(moment().format('D MMM YYYY'))
  }, [])

  const itemActive =
    coursesWaitPayment.filter((i) => (i.STATUS !== 'INACTIVE') & (i.STATUS !== 'CANCEL')) || []
  // console.log('MyCourses', { itemActive, calendars, startDate })

  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <Typography variant="h4" className={classes.TypographyH4}>
          {`Hello, ${profile.FISRT_NAME || ''} ${profile.LAST_NAME || ''}`}
        </Typography>
        {/* <Alert
          classes={{
            root: classes.rootAlert,
            standardError: classes.standardError,
            icon: classes.iconAlert,
            action: classes.actionAlert,
          }}
          severity="error"
          icon={
            <div className={classes.iconBoxAlert}>
              <Icon className={classes.iconStyles}>bell</Icon>
            </div>
          }
          action={
            <div>
              View Now
              <Icon style={{ fontSize: '0.6875rem', marginLeft: '8px' }}>arrowForward</Icon>
            </div>
          }
        >
          This is a success alert — check it out!
        </Alert> */}
        {/** Calendar */}
        <Grid container style={{ marginTop: '32px' }} spacing={2}>
          <Grid item xs={12} md={5} lg={5}>
            <div className={classes.scheduleBox}>
              <Calendar value={startDate} onChange={handleChangeDate} options={calendars} />
            </div>
          </Grid>
          <Grid item xs={12} md={7} lg={7}>
            <div className={classes.scheduleBox}>
              <div className={classes.sectionTitle}>Your Schedule Today</div>
              <ScheduleDetails details={selectCalendar} actions={actions} profile={profile} />
            </div>
          </Grid>
        </Grid>

        {/** Course Lists */}
        {profile && (
          <div className={classes.rootCourseLists}>
            <Typography className={classes.TypographySpan}>
              *Unsuccessful: The date or time you requested is not available. Please select a
              different schedule
            </Typography>
            <Typography variant="h5" className={classes.TypographyH5}>
              Courses
            </Typography>
            
            {itemActive && itemActive.length > 0 && (
              <Panding items={itemActive} actions={actions} />
            )}
            {getStudentUser(profile.TYPE) && (
              <Student
                myCourses={myCourses}
                sessionDetail={sessionDetail}
                accountsStudent={accountsStudent}
                accountsTeacher={accountsTeacher}
                actions={actions}
                openBooking={openBooking}
              />
            )}
            {getAdminUser(profile.TYPE) && (
              <Teacher
                myCourses={myCourses}
                sessionDetail={sessionDetail}
                accountsStudent={accountsStudent}
                accountsTeacher={accountsTeacher}
                actions={actions}
              />
            )}
          </div>
        )}
      </Container>
      {actionIsLoading && <BackdropLoading open={actionIsLoading} />}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    calendarDetails: state.myCourse,
    actionIsLoading: state.isLoading.actionMyCourseLoading,
    openBooking: state.modal.openBooking,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  }
}

const enhancer = compose(nextConnect(mapStateToProps, mapDispatchToProps))

export default enhancer(MyCourses)
