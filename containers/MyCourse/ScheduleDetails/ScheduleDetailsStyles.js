import { makeStyles } from '@material-ui/core/styles'

const ScheduleDetails = makeStyles((theme) => ({
  root: {},
  rootScheduleDetails: {
    marginTop: '16px',
    height: '-webkit-fill-available',
    overflowY: 'auto',
  },
  sectionScheduleDetails: {
    display: 'flex',
    paddingBottom: '12px',
    fontSize: '0.875rem',
    lineHeight: '1.5rem',
    cursor: 'pointer',
    '& .time': {
      padding: '12px 16px',
      color: 'rgba(0, 0, 0, 0.6)',
    },
  },
  sectionDetails: {
    width: '100%',
    background: '#F8F7F7',
    borderRadius: '4px',
    padding: '12px 16px',
    '& .title': {
      fontWeight: 600,
      color: 'rgba(0, 0, 0, 0.7)',
      marginLeft: 5,
    },
    '& .alert': {
      backgroundColor: '#FDE8E9',
      borderRadius: '5px',
      color: theme.palette.primary.main,
      padding: '2px 8px',
      marginLeft: '8px',
    },
    '& .description': {
      lineHeight: '1.25rem',
      color: 'rgba(0, 0, 0, 0.5)',
      marginLeft: 5,
    },
  },
  noEventRoot: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  statusPanding: {
    width: 112,
    background: '#FFEBBA',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#83610D',
    // cursor: 'pointer',
  },
  statusInactive: {
    width: 112,
    background: '#FFD8D8',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#B41B1B',
    // cursor: 'pointer',
  },
  statusRescheduled: {
    width: 112,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#808080',
    // cursor: 'pointer',
    border: '1px solid #F1EFEF',
  },
}))

export default ScheduleDetails
