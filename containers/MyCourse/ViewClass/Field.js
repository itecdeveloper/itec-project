import Select from '@components/Select'
import Input from '@components/Input'
import Autocomplete from '@components/Autocomplete'
import AutocompleteTime from '@components/AutocompleteTime'
import InputDate from '@components/InputDate'

import moment from 'moment'

function clearNumber(value = '') {
  return value.replace(/\D+/g, '')
}

function formatTime(value) {
  const clearValue = clearNumber(value)
  const HH = clearValue.slice(0, 2)
  const MM = clearValue.slice(2, 4)
  if (clearValue.length >= 3) {
    return `${parseInt(HH) < 24 ? HH : '23'}:${parseInt(MM) < 60 ? MM : '59'}`
  }

  return clearValue
}

export function CustomInputTime({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'class_time')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputType({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'type')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputDate({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'date')
  }
  return <InputDate onChange={handleChange} minDate={new Date(moment())} {...rest} />
}

export function CustomInputSession({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'session')
  }
  return <Autocomplete onChange={handleChange} {...rest} />
}

export function CustomInputTimeH({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'time_hour')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}

export function CustomInputTimeM({ onChange, ...rest }) {
  const handleChange = (value) => {
    onChange(value, 'time_min')
  }
  return <AutocompleteTime onChange={handleChange} {...rest} />
}
