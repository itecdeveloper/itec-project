import React, { Fragment, useState } from 'react'
import moment from 'moment'
import { toNumber } from '@util/dataFormate'

import Grid from '@material-ui/core/Grid'

import Modal from '@components/Modal'
import Icon from '@components/Icon'
import IconRounded from '@components/IconRounded'
import Button from '@components/Button'

import PandingStyles, { MakePaymentStyles, ImportantStyles } from './PandingStyles'

function Important() {
  const classes = ImportantStyles()

  return (
    <div className={classes.root}>
      <div style={{ marginBottom: 16 }}>
        <IconRounded
          title="Important"
          icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>messageSquare</Icon>}
        />
      </div>
      <div className={classes.detailTitle}>
        <span className="span1">To confirm your payment, </span>
        <span>please contact us with the following information:</span>
      </div>
      <div className={classes.detailList}>
        <ul>
          <li>Order No.</li>
          <li>Course Name</li>
          <li>Image of your Payment Slip</li>
        </ul>
      </div>
      <div className={classes.detailContact}>
        <span>Contact Channel:</span>
        <Grid container>
          <Grid item xs={3}>
            Line
          </Grid>
          <Grid item xs={9} style={{ fontWeight: 600 }}>
            @eduworld.online
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={3}>
            Email
          </Grid>
          <Grid item xs={9} style={{ fontWeight: 600 }}>
             study@eduworld.ac.th
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

function MakePayment({ open, onClose, items, data, actions }) {
  const classes = MakePaymentStyles()
  const handleCancleOrder = () => {
    onClose((prev) => !prev)
    actions.cancleOrder(data, items)
  }
  // console.log('MakePayment', { data })

  return (
    <Modal
      open={open}
      onClose={() => onClose((prev) => !prev)}
      headerChildren={
        <IconRounded
          title="Pending Course"
          icon={<Icon style={{ fontSize: '1.5rem', overflow: 'unset' }}>doc</Icon>}
        />
      }
    >
      <div className={classes.root}>
        <div style={{ marginBottom: 32 }}>
          <div className={classes.warningDetail}>
            <p>The order will be automatically cancelled if the payment is not made within</p>
            <p style={{ fontWeight: 'bold' }}>
              {moment(data.CREATED_AT)
                .add(data.ORDER_EXPIRATION_TIME, 's')
                .format('MMMM D, YYYY HH:mm')}
            </p>
          </div>
          <Grid container className={classes.gridContainer}>
            <Grid item xs={3}>
              <span className={classes.firstText}>Name</span>
            </Grid>
            <Grid item xs={9}>
              <span className={classes.lastText}>{data.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
            </Grid>
          </Grid>
          {/* <Grid container className={classes.gridContainer}>
            <Grid item xs={3}>
              <span className={classes.firstText}>Level</span>
            </Grid>
            <Grid item xs={9}>
              <span className={classes.lastText}>Level 4: metropolis</span>
            </Grid>
          </Grid> */}
          <Grid container className={classes.gridContainer}>
            <Grid item xs={3}>
              <span className={classes.firstText}>Session</span>
            </Grid>
            <Grid item xs={9}>
              <span className={classes.lastText}>{data.SESSION_QUANTITY}</span>
            </Grid>
          </Grid>
          <Grid container className={classes.gridContainer}>
            <Grid item xs={3}>
              <span className={classes.firstText}>Price</span>
            </Grid>
            <Grid item xs={9}>
              <span className={classes.lastText}>{toNumber(data.PRICE, '0,0')} THB</span>
            </Grid>
          </Grid>
        </div>
        <div style={{ textAlign: 'center' }}>
          <Button
            onClick={() => (window.location = `/payment?order=${data.ORDER_ID}`)}
            color="primary"
            size="large"
            fullWidth
          >
            Make Payment
          </Button>
        </div>
        <div style={{ textAlign: 'center' }}>
          <Button onClick={handleCancleOrder} size="large" fullWidth>
            Cancel Order
          </Button>
        </div>
      </div>
      {data.PAYMENT_TYPE === 'bank_tran' && <Important />}
      {/* <Important /> */}
    </Modal>
  )
}

function Panding({ items, actions }) {
  const [open, setOpen] = useState(false)
  const [data, setData] = useState({})

  const handleMakePayment = (index) => {
    setData(items[index])
    setOpen(true)
  }

  const classes = PandingStyles()
  // console.log('items', { items })

  return (
    <Fragment>
      {items.map((item, index) => (
        <div key={index} className={classes.root}>
          <Grid container alignItems="center" spacing={2}>
            <Grid
              style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}
              item
              xs={4}
              md={7}
            >
              <span className={classes.span}>{item.COURSE_DETAIL.DETAIL.COURSE_NAME}</span>
            </Grid>
            {/* <Grid item xs={5}>
              <span className={classes.span}>Stater</span>
            </Grid> */}
            <Grid item xs={4} md={2}>
              <span className={classes.span}>
                {moment(item.CREATED_AT).add(item.ORDER_EXPIRATION_TIME, 's').format('MMMM	D HH:mm')}
              </span>
            </Grid>
            <Grid item xs={4} md={3}>
              <div className={classes.statusPanding} onClick={() => handleMakePayment(index)}>
                Waiting for payment
              </div>
            </Grid>
          </Grid>
        </div>
      ))}
      {Object.keys(data).length > 0 && (
        <MakePayment open={open} onClose={setOpen} items={items} data={data} actions={actions} />
      )}
    </Fragment>
  )
}

export default Panding
