import React, { Fragment, useState } from 'react'
import classNames from 'classnames'
import moment from 'moment'
import { toDateTimeFormat } from '@util/dataFormate'

import Paper from '@material-ui/core/Paper'
import Icon from '@components/Icon'
import TableContainer from '@material-ui/core/TableContainer'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import TablePagination from '@material-ui/core/TablePagination'
import Box from '@material-ui/core/Box'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

import datatableStyles from './DatatableStyled'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

function StatusComponent({ details }) {
  const classes = datatableStyles()

  if (details.S_STATUS === 'WAITING_APPROVE') {
    return (
      <div style={{ marginLeft: 'auto' }} className={classes.statusPanding}>
        Pending
      </div>
    )
  } else {
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'req') {
      return (
        <div style={{ marginLeft: 'auto' }} className={classes.statusPanding}>
          Pending
        </div>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return (
        <div style={{ marginLeft: 'auto' }} className={classes.statusRescheduled}>
          Rescheduled
        </div>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return (
        <div style={{ marginLeft: 'auto' }} className={classes.statusInactive}>
          Unsuccessful
        </div>
      )
    } else {
      return null
    }
  }
}

function DisplayTime({ course }) {
  const { details } = course
  const date = toDateTimeFormat(course.date)
  const classes = datatableStyles()

  if (details.STATUS_NEW_START_TIME) {
    const newDate = toDateTimeFormat(details.NEW_START_TIME)
    if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'approve') {
      return (
        <span
          className={classes.displayTimeRoot}
        >{`${date.date} • ${date.time.hour}:${date.time.min}`}</span>
      )
    } else if (details.STATUS_NEW_START_TIME && details.STATUS_NEW_START_TIME === 'reject') {
      return (
        <Fragment>
          <span
            className={classes.displayTimeRoot}
          >{`${date.date} • ${date.time.hour}:${date.time.min}`}</span>
          <Icon style={{ fontSize: 8, marginLeft: 5, marginRight: 5 }}>arrowForward</Icon>
          <span
            style={{ fontSize: '13px', lineHeight: '20px', textDecoration: 'line-through' }}
          >{`${newDate.date} • ${newDate.time.hour}:${newDate.time.min}`}</span>
        </Fragment>
      )
    } else {
      return (
        <Fragment>
          <span
            style={{ fontSize: '13px', lineHeight: '20px' }}
          >{`${date.date} • ${date.time.hour}:${date.time.min}`}</span>
          <Icon style={{ fontSize: 8, marginLeft: 5, marginRight: 5 }}>arrowForward</Icon>
          <span
            className={classes.displayTimeRoot}
          >{`${newDate.date} • ${newDate.time.hour}:${newDate.time.min}`}</span>
        </Fragment>
      )
    }
  } else {
    return (
      <span
        className={classes.displayTimeRoot}
      >{`${date.date} • ${date.time.hour}:${date.time.min}`}</span>
    )
  }
}

function EnhancedTableHead({ keyField, headCells, order, orderBy, onRequestSort, type }) {
  const classes = datatableStyles()
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell, index) => (
          <TableCell
            classes={{ head: classes.headTableCell }}
            key={headCell.id}
            align={headCell.alignCell ? headCell.alignCell : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            {headCell.sort ? (
              <TableSortLabel
                classes={{
                  root: classes.rootTableSortLabel,
                  active: classes.activeTableSortLabel,
                  icon: classes.iconTableSortLabel,
                }}
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                IconComponent={ArrowDropDown}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
        {type !== 'teacher' && <TableCell classes={{ head: classes.headTableCell }} />}
        <TableCell classes={{ head: classes.headTableCell }} />
      </TableRow>
    </TableHead>
  )
}

function EnhancedTableBody(props) {
  const { row, type, index, handleChangeStatus, handleViewSession } = props
  const [open, setOpen] = useState(false)
  const classes = datatableStyles({ open })

  return (
    <Fragment>
      <TableRow className={classes.rootTableRow}>
        <TableCell
          component="th"
          scope="row"
          classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}
        >
          {row.name}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.levelName}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.student}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.expireDate}
        </TableCell>
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.status}
        </TableCell>
        {type !== 'teacher' && (
          <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
            {row.element}
          </TableCell>
        )}
        <TableCell classes={{ root: classes.rootTableCell, body: classes.bodyTableCell }}>
          {row.sessions.length > 0 && (
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
              className={classes.rootIconButton}
            >
              {open ? (
                <Icon className={classes.iconExpand}>arrowDownward</Icon>
              ) : (
                <Icon className={classes.iconExpand}>arrowForward</Icon>
              )}
            </IconButton>
          )}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0 }} colSpan={type === 'teacher' ? 6 : 7}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box>
              <Table size="small" aria-label="purchases">
                <TableBody>
                  {row.sessions.map((course, key) => {
                    // console.log(
                    //   'diff',
                    //   moment(course.details.START_TIME).format('MM/DD/YYYY'),
                    //   moment().diff(moment(course.details.COMPLETED_AT), 'days'),
                    // )
                    return (
                      <TableRow
                        key={course.date}
                        style={{
                          backgroundColor: course.status === 'completed' ? '#F4F4F4' : '#FFF',
                        }}
                      >
                        <TableCell
                          style={{ width: 89, minWidth: 89 }}
                          classes={{
                            root:
                              course.status === 'completed'
                                ? classes.smRootTableCellCompleted
                                : classes.smRootTableCell,
                            sizeSmall: classes.sizeSmallTableCell,
                          }}
                          component="th"
                          scope="row"
                        >
                          {key + 1}
                        </TableCell>
                        <TableCell
                          style={{ width: 300, minWidth: 300 }}
                          classes={{
                            root:
                              course.status === 'completed'
                                ? classes.smRootTableCellCompleted
                                : classes.smRootTableCell,
                            sizeSmall: classes.sizeSmallTableCell,
                          }}
                        >
                          <DisplayTime course={course} />
                        </TableCell>
                        <TableCell
                          style={{ width: '100%' }}
                          align={'left'}
                          classes={{
                            root:
                              course.status === 'completed'
                                ? classes.smRootTableCellCompleted
                                : classes.smRootTableCell,
                            sizeSmall: classes.sizeSmallTableCell,
                          }}
                        >
                          {course.text}
                        </TableCell>
                        <TableCell
                          classes={{
                            root:
                              course.status === 'completed'
                                ? classes.smRootTableCellCompleted
                                : classes.smRootTableCell,
                            sizeSmall: classes.sizeSmallTableCell,
                          }}
                        >
                          {type === 'teacher' ? (
                            <div class="custom-select">
                              <select
                                className={
                                  course.status === 'completed'
                                    ? classes.selectRootComplete
                                    : classes.selectRoot
                                }
                                value={course.status}
                                onChange={({ target }) =>
                                  handleChangeStatus(target.value, course.orderId, course.sessionId)
                                }
                                disabled={
                                  moment().valueOf() > course.details.END_TIME
                                    ? course.details.COMPLETED_AT === 0
                                      ? false
                                      : moment().diff(moment(course.details.COMPLETED_AT), 'days') >
                                        1
                                      ? true
                                      : false
                                    : true
                                }
                              >
                                <option value="completed">Completed</option>
                                <option value="incomplete">Incomplete</option>
                              </select>
                            </div>
                          ) : (
                            <StatusComponent details={course.details} />
                          )}
                        </TableCell>
                        <TableCell
                          align={'right'}
                          classes={{
                            root:
                              course.status === 'completed'
                                ? classes.smRootTableCellCompleted
                                : classes.smRootTableCell,
                            sizeSmall: classes.sizeSmallTableCell,
                          }}
                          style={{ cursor: 'pointer', width: 120, minWidth: 120 }}
                          onClick={() => handleViewSession(course.orderId, course.sessionId)}
                        >
                          View Class
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </Fragment>
  )
}

function Datatable({ keyField, columns, data, type, actions }) {
  const classes = datatableStyles()
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('name')
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleChangeStatus = (value, orderId, sessionId) => {
    // console.log('handleChangeStatus', { value, orderId, sessionId })
    actions.statusSession({ value, orderId, sessionId })
  }

  const handleViewSession = (orderId, sessionId) => {
    // console.log('handleViewSession', { orderId, sessionId })
    actions.viewSession(orderId, sessionId)
  }

  return (
    <Fragment>
      <TableContainer component={Paper} className={classes.paper}>
        <Table aria-label="collapsible table">
          <EnhancedTableHead
            keyField={keyField}
            headCells={columns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            type={type}
          />
          <TableBody>
            {stableSort(data, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                return (
                  <EnhancedTableBody
                    key={index}
                    row={row}
                    type={type}
                    index={index}
                    handleChangeStatus={handleChangeStatus}
                    handleViewSession={handleViewSession}
                  />
                )
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Fragment>
  )
}
export default Datatable
