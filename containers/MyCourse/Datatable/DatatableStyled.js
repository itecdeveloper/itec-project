import { makeStyles } from '@material-ui/core/styles'

const datatableStyle = makeStyles((theme) => ({
  paper: {
    boxShadow: 'unset',
    borderRadius: 'unset',
  },
  iconExpand: {
    color: theme.palette.primary.main,
    fontSize: '1rem',
    cursor: 'pointer',
    overflow: 'unset',
  },
  rootTableRow: {
    '&:hover': {
      '& $rootIconButton': {
        opacity: 1,
      },
    },
  },
  rootTableCell: {
    fontWeight: 'normal',
    lineHeight: '1.25rem',
    borderBottom: 'unset',
  },
  headTableCell: {
    fontSize: 12,
    fontWeight: 600,
    lineHeight: '1.125rem',
    color: 'rgba(0, 0, 0, 0.56)',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  bodyTableCell: {
    color: 'rgba(0, 0, 0, 0.6)',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  sizeSmallTableCell: {
    padding: '10px',
    color: '#7A7A7A',
    fontSize: '0.8125rem',
    fontWeight: 'normal',
    '&:first-child': {
      paddingLeft: '24px',
    },
    '&:last-child': {
      paddingRight: '24px',
    },
  },
  smRootTableCell: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.06)',
  },
  smRootTableCellCompleted: {
    borderBottom: '1px solid #F4f4f4',
  },
  rootIconButton: {
    opacity: 1,
    // opacity: ({ open }) => (open ? 1 : 0),
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rootTableSortLabel: {
    '&$activeTableSortLabel': {
      color: 'rgba(0, 0, 0, 0.56)',
    },
  },
  activeTableSortLabel: {},
  iconTableSortLabel: { opacity: 1 },
  buttonView: {
    fontSize: '13px',
  },
  selectRoot: {
    color: 'rgba(0, 0, 0, 0.6)',
    borderRadius: 5,
    border: '1px solid #CCCCCC',
    padding: '2px 8px',
  },
  selectRootComplete: {
    color: '#FFFFFF',
    borderRadius: 5,
    border: '1px solid rgba(70, 181, 50, 0.7)',
    background: 'rgba(70, 181, 50, 0.7)',
    padding: '2px 8px',
  },
  statusPanding: {
    width: 112,
    background: '#FFEBBA',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#83610D',
    // cursor: 'pointer',
  },
  statusInactive: {
    width: 112,
    background: '#FFD8D8',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#B41B1B',
    // cursor: 'pointer',
  },
  statusRescheduled: {
    width: 112,
    background: '#FFFFFF',
    borderRadius: 5,
    padding: '4px 13px',
    fontWeight: 600,
    fontSize: 13,
    lineHeight: '20px',
    textAlign: 'center',
    color: '#808080',
    // cursor: 'pointer',
    border: '1px solid #F1EFEF',
  },
  displayTimeRoot: {
    fontWeight: 600,
    fontSize: '13px',
    lineHeight: '20px',
    color: 'rgba(0, 0, 0, 0.6)',
  },
}))

export default datatableStyle
