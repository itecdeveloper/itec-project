import React, { useState } from 'react'
import moment from 'moment'
import { expanSessionFormatTeacher, statusSessionFormat } from '@util/myCourseFunc'
import { sortFunc } from '@util/dataFormate'
import { finderLevelId } from '@util/filterData'

import Grid from '@material-ui/core/Grid'

import Modal from '@components/Modal'
import Icon from '@components/Icon'

import Datatable from '../Datatable'
import ViewClass from '../ViewClass'

function mapStudentName(studentId, datas) {
  const student = datas.find((data) => data.ACCOUNT_ID === studentId)
  return `${student?.FISRT_NAME} ${student?.LAST_NAME}`
}

function createData(name, levelId, student, expireDate, status, { SESSIONS, LEVELS }) {
  const level = finderLevelId(LEVELS, levelId)
  return {
    name,
    levelName: level?.LEVEL || '-',
    student,
    expireDate,
    status: statusSessionFormat(SESSIONS, status),
    sessions: expanSessionFormatTeacher(SESSIONS, LEVELS),
  }
}

const columns = [
  { id: 'name', sort: true, label: 'Course Name' },
  { id: 'level', label: 'Level' },
  { id: 'student', label: 'Student' },
  { id: 'expireDate', label: 'Expiry Date' },
  { id: 'status', sort: true, label: 'Status' },
]

function Teacher({ myCourses, sessionDetail, accountsStudent, actions }) {
  const [openBook, setOpenBook] = useState(false)

  const rows = myCourses
    .map((course, index) =>
      createData(
        course.COURSE_DETAIL.DETAIL.COURSE_NAME,
        course.LEVEL_ID,
        mapStudentName(course.STUDENT_ID, accountsStudent),
        moment(
          parseInt(course.START_TIME + course.COURSE_DETAIL.DETAIL.ORDER_EXPIRATION_TIME * 1000),
        ).format('MMMM, DD YYYY'),
        course.SESSION_QUANTITY,
        { SESSIONS: course.SESSIONS, LEVELS: course.COURSE_DETAIL.LEVEL },
      ),
    )
    .filter((i) => i.sessions.length > 0)

  // console.log('myCourses', {
  //   myCourses: myCourses,
  //   rows,
  // })

  return (
    <div>
      <Datatable
        type="teacher"
        keyField="tableCourse"
        columns={columns}
        data={rows}
        expandRow="right"
        actions={actions}
      />
      <ViewClass type="teacher" />
    </div>
  )
}

export default Teacher
