import React, { useState } from 'react'
import moment from 'moment'
import { expanSessionFormat, statusSessionFormat } from '@util/myCourseFunc'
import { finderLevelId } from '@util/filterData'

import Button from '@components/Button'

import Booking from '@containers/Booking'

import Datatable from '../Datatable'
import ViewClass from '../ViewClass'

function mapStudentName(studentId, datas) {
  const student = datas.find((data) => data.ACCOUNT_ID === studentId)
  return student ? `${student?.FISRT_NAME} ${student?.LAST_NAME}` : '-'
}

function createData(name, levelId, student, expireDate, status, element, { SESSIONS, LEVELS }) {
  const level = finderLevelId(LEVELS, levelId)
  return {
    name,
    levelName: level?.LEVEL || '-',
    student,
    expireDate,
    status: statusSessionFormat(SESSIONS, status),
    element,
    sessions: expanSessionFormat(SESSIONS, LEVELS),
  }
}

const columns = [
  { id: 'name', sort: true, label: 'Course Name' },
  { id: 'level', label: 'Level' },
  { id: 'teacher', label: 'Teacher' },
  { id: 'expireDate', label: 'Expiry Date' },
  { id: 'status', label: 'Status' },
]

function Student({
  myCourses,
  sessionDetail,
  accountsStudent,
  accountsTeacher,
  actions,
  openBooking,
}) {
  const [openBook, setOpenBook] = useState(false)

  const handleClickBook = async (index) => {
    await actions.initSession(index)
    actions.setOpenBookingModal(true)
  }
  const rows = myCourses.map((course, index) => {
    const sessions = course.SESSIONS.filter((i) => i.S_STATUS !== 'INACTIVE')
    const now = moment().valueOf()
    const expTime = parseInt(
      course.START_TIME + course.COURSE_DETAIL.DETAIL.ORDER_EXPIRATION_TIME * 1000,
    )
    return createData(
      course.COURSE_DETAIL.DETAIL.COURSE_NAME,
      course.LEVEL_ID,
      mapStudentName(course.TEACHER_ID, accountsTeacher),
      moment(
        parseInt(course.START_TIME + course.COURSE_DETAIL.DETAIL.ORDER_EXPIRATION_TIME * 1000),
      ).format('MMMM, DD YYYY'),
      course.SESSION_QUANTITY,
      <Button
        style={{
          display: course.SESSION_QUANTITY === sessions.length ? 'none' : now > expTime && 'none',
        }}
        onClick={() => handleClickBook(index)}
        color="primary"
        size="small"
      >
        Booking
      </Button>,
      { SESSIONS: course.SESSIONS, LEVELS: course.COURSE_DETAIL.LEVEL },
    )
  })
  // console.log('myCourses', {
  //   myCourses: myCourses,
  //   rows,
  // })

  return (
    <div>
      <Datatable
        keyField="tableCourse"
        columns={columns}
        data={rows}
        expandRow="right"
        actions={actions}
      />
      <Booking
        sessionDetail={sessionDetail}
        open={openBooking}
        onClose={() => actions.setOpenBookingModal(false)}
      />
      <ViewClass />
    </div>
  )
}

export default Student
