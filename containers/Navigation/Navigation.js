import React, { useRef, useState } from 'react'
import dynamic from 'next/dynamic'
import { bindActionCreators } from 'redux'
import { nextConnect } from '@redux/store'
import * as modalActions from '@actions/modalActions'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
// import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Popper from '@material-ui/core/Popper'
import Paper from '@material-ui/core/Paper'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Slide from '@material-ui/core/Slide'
import Fade from '@material-ui/core/Fade'
import PersonOutlineIcon from '@material-ui/icons/PersonOutline'
import Typography from '@material-ui/core/Typography'
import { Button as MuiButton } from '@material-ui/core'
import NoSsr from '@material-ui/core/NoSsr'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import MenuBookIcon from '@material-ui/icons/MenuBook'

// import Button from '@components/Button'
import Link from '@components/Link'
import Icon from '@components/Icon'

import { useMember } from '@containers/auth'

import NavigationStyles from './NavigationStyled'

import { getStatic } from '@lib/static'

import { signOut } from '@services/cookie'

const Button = dynamic(() => import('@components/Button'), {
  ssr: false,
  loading: () => null,
})
const IconButton = dynamic(() => import('@material-ui/core/IconButton'), {
  ssr: false,
  loading: () => null,
})

function MenuAccount({ profile }) {
  const [anchorEl, setAnchorEl] = useState(null)

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popper' : undefined

  const classes = NavigationStyles()

  return (
    <div style={{ borderLeft: '1px solid #CCCCCC', paddingLeft: 15 }}>
      <IconButton className={classes.buttonUser} onClick={handleClick} aria-describedby={id}>
        <Icon style={{ fontSize: '2.5rem', overflow: 'unset' }}>user</Icon>
      </IconButton>
      <Popper
        className={classes.popperAccount}
        id={id}
        open={open}
        anchorEl={anchorEl}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Fade {...TransitionProps} timeout={500}>
            <Paper id="menu-list-growUser" className={classes.paperAccount}>
              <div className={classes.mobileRegisterRoot}>
                <div className={classes.account}>
                  <Typography className="title" variant="h5">
                    {profile?.FISRT_NAME || ''}
                  </Typography>
                  <Typography className="email">{profile?.EMAIL || ''}</Typography>
                </div>
                <div>
                  <Link route="profile" passHref>
                    <MuiButton className={classes.muiButton}>
                      <PersonOutlineIcon style={{ marginRight: 16 }} /> Edit Profile
                    </MuiButton>
                  </Link>
                </div>
                <div>
                  <Link route="my-course" passHref>
                    <MuiButton className={classes.muiButton}>
                      <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 16 }}>
                        bookmark
                      </Icon>
                      My Courses
                    </MuiButton>
                  </Link>
                </div>
                <div>
                  <MuiButton className={classes.muiButton} onClick={() => signOut()}>
                    <Icon style={{ fontSize: '1.5rem', overflow: 'unset', marginRight: 16 }}>
                      logout
                    </Icon>
                    Log Out
                  </MuiButton>
                </div>
              </div>
            </Paper>
          </Fade>
        )}
      </Popper>
    </div>
  )
}

function NavigationMobile({ modal, modalActions }) {
  const [menu, setMenu] = useState(false)
  const anchorRef = useRef(null)
  const { open } = modal
  const { isAuthenticated, profile } = useMember()

  const handleToggle = () => {
    event.preventDefault()
    setMenu((prevMenu) => !prevMenu)
    // pageConfigAction.setReponsiveToolbarOpen(menu)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setMenu(false)
    // pageConfigAction.setReponsiveToolbarOpen(true)
  }
  // const opacity = menu
  const classes = NavigationStyles({ opacity: menu })

  return isAuthenticated ? (
    <div className={classes.iconButtonRoot}>
      <MenuAccount profile={profile} />
    </div>
  ) : (
    <div className={classes.iconButtonRoot}>
      <IconButton
        onClick={handleToggle}
        aria-owns={menu ? 'menu-list-user' : undefined}
        aria-haspopup="true"
      >
        <MenuIcon fontSize="small" />
      </IconButton>
      <Popper
        className={classes.popperFullWidth}
        open={menu}
        // anchorEl={anchorRef.current}
        // placement="bottom"
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Fade {...TransitionProps} timeout={500}>
            <Paper id="menu-list-growUser" className={classes.paper}>
              <ClickAwayListener onClickAway={handleClose}>
                <div className={classes.mobileRegisterRoot}>
                  <Button
                    color="primary"
                    size="medium"
                    onClick={() => {
                      modalActions.setOpenModal(!open)
                      modalActions.setMobileStep('register')
                    }}
                    fullWidth
                  >
                    Register
                  </Button>
                  <div className={classes.mobileLoginRoot}>
                    <span className="text">Already have an account?</span>
                    <span
                      className="text-login"
                      onClick={() => {
                        modalActions.setOpenModal(!open)
                        modalActions.setMobileStep('login')
                      }}
                    >
                      Log In
                    </span>
                  </div>
                </div>
              </ClickAwayListener>
            </Paper>
          </Fade>
        )}
      </Popper>
    </div>
  )
}

function Navigation({ modal, modalActions }) {
  const { open } = modal
  const classes = NavigationStyles()
  const { isAuthenticated, profile } = useMember()

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="fixed" color="inherit">
        <Toolbar className={classes.toolBar}>
          <div className={classes.title}>
            <Link route="home" passHref>
              <img
                // src={`${getStatic('static/images/Eduworld.png')}`}
                src="https://itec-public-file.s3-ap-southeast-1.amazonaws.com/ivcon.png"
                alt="img"
                style={{ width: 48, cursor: 'pointer' }}
              />
            </Link>
          </div>
          <NoSsr>
            <div className={classes.buttonRoot}>
              {isAuthenticated ? (
                <MenuAccount profile={profile} />
              ) : (
                <Button
                  color="primary"
                  size="medium"
                  onClick={() => modalActions.setOpenModal(!open)}
                >
                  Login
                </Button>
              )}
            </div>
          </NoSsr>

          <NoSsr>
            <NavigationMobile modal={modal} modalActions={modalActions} />
          </NoSsr>
        </Toolbar>
      </AppBar>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    modalActions: bindActionCreators(modalActions, dispatch),
  }
}

export default nextConnect(mapStateToProps, mapDispatchToProps)(Navigation)
