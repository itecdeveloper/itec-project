import React, { useState } from 'react'
import { useForm } from 'react-hook-form'

import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import FormLabel from '@material-ui/core/FormLabel'

import Input from '@components/Input'
import Button from '@components/Button'
import Terms from '@components/Terms'

import RegisterStyles from './RegisterStyles'
import { schemaFirstForm, schemaValidate } from './validate'

function FirstForm({ actions }) {
  const [term, setTerm] = useState(false)
  const { register, handleSubmit, errors, watch } = useForm({
    validationResolver: schemaValidate,
    validationContext: schemaFirstForm,
  })
  const onSubmit = (data) => {
    // console.log(data)
    if (watch('AGREE')) {
      actions.setRegisterDetails(data)
      actions.setPage(2)
    }
  }

  const classes = RegisterStyles()

  return (
    <div className={classes.firstFormRoot}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.inputRoot}>
          <Input
            id="email-input"
            name="EMAIL"
            label="Email"
            className={classes.margin}
            inputRef={register}
            error={errors.EMAIL}
            errorText={errors.EMAIL && errors.EMAIL.message}
          />
        </div>
        <div className={classes.inputRoot}>
          <Input
            id="password-input"
            name="PASSWORD"
            label="Password"
            type="password"
            className={classes.margin}
            inputRef={register}
            error={errors.PASSWORD}
            errorText={errors.PASSWORD && errors.PASSWORD.message}
          />
        </div>
        <div className={classes.inputRoot}>
          <Input
            id="confirm-password-input"
            name="RE_PASSWORD"
            label="Confirm Password"
            type="password"
            className={classes.margin}
            inputRef={register}
            error={errors.RE_PASSWORD}
            errorText={errors.RE_PASSWORD && errors.RE_PASSWORD.message}
          />
        </div>
        <div className={classes.inputRoot}>
          <FormControlLabel
            className={classes.formControlLabel}
            classes={{
              label: classes.labelCheck,
            }}
            control={
              <Checkbox
                name="AGREE"
                inputRef={register}
                className={classes.radioRoot}
                classes={{
                  checked: classes.checked,
                }}
              />
            }
            label="Agree to "
          />{' '}
          <FormLabel className={classes.terms} onClick={() => setTerm((prev) => !prev)}>
            Terms & Conditions
          </FormLabel>
        </div>
        <div className={classes.buttonRoot}>
          <Button type="submit" color="primary" size="large" disabled={!watch('AGREE')}>
            Continue
          </Button>
        </div>
      </form>
      <Terms open={term} onClose={() => setTerm((prev) => !prev)} />
    </div>
  )
}

export default FirstForm
