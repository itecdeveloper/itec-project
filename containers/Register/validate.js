import * as yup from 'yup'
import moment from 'moment'
import { ValidateEmail } from '@actions/registerActions'

export const validateAge = (watchDateOfBirth) => {
  const now = moment().diff(moment(watchDateOfBirth, 'DD/MM/YYYY'), 'years')
  return now
}

export const schemaFirstForm = ({ PASSWORD }) =>
  yup.object().shape({
    EMAIL: yup
      .string()
      .test('EMAIL', 'Email Duplicate', async (value) => await ValidateEmail(value))
      .email('Invalid Email')
      .required('this field is a required'),
    PASSWORD: yup
      .string()
      // .matches(/^\S*$/, 'not allowing certain special characters')
      // .matches(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,}$/g, 'not allowing certain special characters')
      .min(8, 'password must be 8 characters')
      .required('this field is a required'),
    RE_PASSWORD: yup
      .string()
      // .matches(/^\S*$/, 'not allowing certain special characters')
      // .matches(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,}$/g, 'not allowing certain special characters')
      .oneOf([PASSWORD], 'confirm password must be one of the password')
      .required('this field is a required'),
  })

export const schemaSecondForm = () =>
  yup.object().shape({
    FISRT_NAME: yup
      .string()
      .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
      .required('This field is a required'),
    LAST_NAME: yup
      .string()
      .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
      .required('This field is a required'),
    NICK_NAME: yup
      .string()
      .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
      .required('This field is a required'),
    CP_FISRT_NAME: yup
      .string()
      .when('DOB', (DOB, schema) =>
        validateAge(DOB) > 15
          ? schema.optional()
          : schema
              .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
              .required('This field is a required'),
      ),
    CP_LAST_NAME: yup
      .string()
      .when('DOB', (DOB, schema) =>
        validateAge(DOB) > 15
          ? schema.optional()
          : schema
              .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
              .required('This field is a required'),
      ),
    RELATIONSHIP: yup
      .string()
      .when('DOB', (DOB, schema) =>
        validateAge(DOB) > 15
          ? schema.optional()
          : schema
              .matches(/[a-zA-Z0-9ก-๏\-]/, 'This field is a required')
              .required('This field is a required'),
      ),
    CONTACT_NO: yup
      .string()
      .matches(/[a-zA-Z0-9]/, 'This field is a required')
      .required('This field is a required'),
    CP_CONTACT_NO: yup
      .string()
      .when('DOB', (DOB, schema) =>
        validateAge(DOB) > 15
          ? schema.optional()
          : schema
              .matches(/[a-zA-Z0-9]/, 'This field is a required')
              .required('This field is a required'),
      ),
    DOB: yup.string().required('This field is a required'),
  })

export const schemaValidate = async (data, schema) => {
  // console.log('schemaValidate', { data, schema })
  return await schema(data)
    .validate(data, { abortEarly: false })
    .then((res) => {
      // console.log('res', { res })
      return { values: res, errors: {} }
    })
    .catch((err) => {
      // console.log('err', err)
      const errors = err.inner.reduce((allErrors, currentError) => {
        return {
          ...allErrors,
          [currentError.path]: {
            type: currentError.type ?? 'validation',
            message: currentError.message,
          },
        }
      }, {})
      return {
        values: {},
        errors,
      }
    })
}
