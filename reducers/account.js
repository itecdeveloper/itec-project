import React from 'react'
import { getCookies, setCookie } from '@services/cookie'

const initState = getCookies()
const account = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ACCOUNT':
      return setCookie(action.payload)
    default:
      return state
  }
}

export default account
