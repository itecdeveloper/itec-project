const initState = {
  accessAccount: false,
  allCoursesLoading: false,
  courseLoading: false,
  orderLoading: false,
  forgotPasswordLoading: false,
  getOrderLoading: false,
  actionMyCourseLoading: false,
  createSessionLoading: false,
  instituteBannerLoading: false,
}
const isLoading = (state = initState, action) => {
  switch (action.type) {
    case 'SET_ACCESS_ACCOUNT_LOADING':
      return {
        ...state,
        accessAccount: action.isLoading,
      }
    case 'SET_ALL_COURSES_LOADING':
      return {
        ...state,
        allCoursesLoading: action.isLoading,
      }
    case 'SET_COURSE_LOADING':
      return {
        ...state,
        courseLoading: action.isLoading,
      }
    case 'SET_ORDER_LOADING':
      return {
        ...state,
        orderLoading: action.isLoading,
      }
    case 'SET_FORGOT_PASSWORD_LOADING':
      return {
        ...state,
        forgotPasswordLoading: action.isLoading,
      }
    case 'SET_GET_ORDER_LOADING':
      return {
        ...state,
        getOrderLoading: action.isLoading,
      }
    case 'SET_MY_COURSE_LOADING':
      return {
        ...state,
        actionMyCourseLoading: action.isLoading,
      }
    case 'SET_CREATE_SESSION_LOADING':
      return {
        ...state,
        createSessionLoading: action.isLoading,
      }
    case 'SET_INSTITUTE_BANNER_LOADING':
      return {
        ...state,
        instituteBannerLoading: action.isLoading,
      }
    default:
      return state
  }
}

export default isLoading
