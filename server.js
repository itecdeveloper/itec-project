require('dotenv').config()

const path = require('path')
const express = require('express')
const compression = require('compression')
const next = require('next')
const favicon = require('serve-favicon')
const useragent = require('express-useragent')
const routes = require('./router')
const to_json = require('xmljson').to_json
const bodyParser = require('body-parser')

const port = process.env.PORT || 3000

const app = next({
  dev: process.env.NODE_ENV !== 'production',
})

const handle = routes.getRequestHandler(app)

app.prepare().then(() => {
  const server = express()

  server.use(
    compression({
      filter: function (req, res) {
        if (process.env.ASSET_PREFIX !== '') {
          return false
        }

        return compression.filter(req, res)
      },
    }),
  )

  server.use(bodyParser.json()) // to support JSON-encoded bodies
  server.use(
    bodyParser.urlencoded({
      // to support URL-encoded bodies
      extended: true,
    }),
  )

  server.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
  server.use(useragent.express())

  // server.use((req, res) => {
  //   handle(req, res)
  // })

  server.post('/2C2P-response', (req, res, next) => {
    const order_id = req.body.user_defined_1
    const payment_status = req.body.payment_status
    console.log(req.body)
    // res.redirect(`/thankyou?order_id=${order_id}`)
    if (payment_status == '000') {
      res.redirect(`/thankyou?order=${order_id}`)
    } else {
      res.redirect(`/payment?order=${order_id}`)
    }
  })

  server.post('*', (req, res, next) => {
    res.redirect(`${req.path}`)
  })

  server.get('*', (req, res) => {
    handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
