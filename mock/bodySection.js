export default [
  {
    id: 1,
    level: 'Level 1',
    title: 'Take Off',
    time: '[30 hrs]',
    content: 'เริ่มต้นเรียนรู้ไวยากรณ์และคำศัพท์ขั้นพื้นฐาน ประโยคสั้นๆ ทั่วไปได้',
  },
  {
    id: 2,
    level: 'Level 2',
    title: 'Adventure',
    time: '[28 hrs]',
    content: 'สามารถสื่อสารภาษาอังกฤษ ประโยคพื้นฐานในชีวิตประจำวันได้',
  },
  {
    id: 3,
    level: 'Level 3',
    title: 'City Walk',
    time: '[26 hrs]',
    content: 'ผู้เรียนสามารถพูดคุย โต้ตอบบทสนทนาได้อย่างถูกต้อง เหมาะสม',
  },
  {
    id: 4,
    level: 'Level 4',
    title: 'Metropolis',
    time: '[24 hrs]',
    content: 'สามารถสื่อสารและเข้าใจบทสนทนาภาษาอังกฤษยากๆ ได้ มีความคล่องแคล่วมากยิ่งขึ้น',
  },
]
